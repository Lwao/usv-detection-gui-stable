# Graphical user interface oriented to ultrasonic vocalizations detection algorithm

## How to execute

1. To run the software [chronux package](http://chronux.org/) must to be in the path list;
2. All of the folders and subfolders of the project also must to be in the path list;
3. To execute the software run the command line:

        >> Detection


## Program flow

In Detection:

- Load and select the audio file to work with;
- Set parameters to spectrogram and detection of the USVs (you can also load previously saved parameters);
- Click the button GENERATE to create the spectrogram;
- Click the button DETECT to detect the USVs by its entropy levels;
- Click the button GO TO CURATING to change the window.

In Curating:

- Redefine the rectangules around the USVs;
- Reject undesirable USVs;
- Add new USVs;
- Save or load session;
- Save calls to send to Deep Squeak program;
- The button CURATE NEW USVS lead to other window to refine the detection of new USVs.

In Refine:

- Same as Curating, but can curate new previously marked USVs .

In Plot:

- Can select any kind of plot: spectrogram, entropy and audio data;
- Can select filters to plot: frequency and time range, specifics and/or random USVs;

In Analysis:

- Obtain data about the model performance of the detection algorithm

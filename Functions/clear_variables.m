function clear_variables()
    root = getappdata(0);
    root_names = fieldnames(root);
    for i = 12:length(root_names)
        rmappdata(0,root_names{i});
    end
end
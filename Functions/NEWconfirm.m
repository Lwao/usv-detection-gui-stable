function [curated_usvs] = NEWconfirm(window_Begin, curated_usvs, k, h)
    curated_usvs(k,1) = k;
    curated_usvs(k,[2:5]) = getPosition(h);
    curated_usvs(k,2) = curated_usvs(k,2) - window_Begin(k);
    curated_usvs(k,6) = 1;
end
h = plot(1:100)
set(gca, 'Xtick', [0 25 50 75 100]);
set(gca, 'Xticklabel', [0 25 50 75 100]);
addlistener(h,'MarkedClean',@(~,~) h.cleanListen);
get(gca,{'xlim','ylim'})

ax = gca;
plot(1:100)
L1 = addlistener(ax, 'XLim',  'PostSet', @(~,~)cleanListen(ax));
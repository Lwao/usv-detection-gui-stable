function curated_usvs = deleteUSV(curated_usvs, k)
    curated_usvs(k,1) = k;
    curated_usvs(k,6) = 0;
end
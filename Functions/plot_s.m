function plot_s(S_temp1, n, t, t1, t2, f, color)
    if(n~=1)
       hold on 
    end
    dur_i = 1;
    dur_f = 0;
    line_end = [];
    intervals = [];
    for i = 1:n
        dur_f = dur_f+length(t(t1(i):t2(i)));
        imagesc(dur_i:dur_f,f/1000,[S_temp1{i}]);
        line_end(i) = dur_f;
        dur_i = dur_f+1;
        intervals = [intervals t(t1(i):t2(i))];
    end
    setappdata(0, 'intervals', intervals);
    % linhas
    for i = 1:length(line_end)
        line([line_end(i) line_end(i)],[20 120],'Color',[0 1 0],'LineStyle','--'); 
    end
    % labels
    tick = [];
    tick_label = [];
    tt = [];
    div = 4;
%     mais = [line_end(1:end)];
%     menos = [0 line_end(1:end-1)];
%     result = mais-menos;
%     ground = floor(result/min(result))
    for i = 1:n
        %div = 3+ground(i);
        if(i==1)
            step = line_end(i)/div;
            tick = [tick 0:step:line_end(i)];
        else
            step = (line_end(i)-line_end(i-1))/div;
            tick = [tick line_end(i-1)+step:step:line_end(i)];
        end
        dur = t(t2(i))-t(t1(i));
        tick_step = round(dur/div,2);
        if(i==1)
            tick_label = [tick_label round(t(t1(i)),2):tick_step:round(t(t2(i)),2)];
        else
            tick_label = [tick_label(1:end-1) round(t(t1(i)),2):tick_step:round(t(t2(i)),2)];
        end 
        if(length(tick_label)<length(tick))
            tick_label = [tick_label tick_label(end)+tick_label(2)-tick_label(1)];
        end
    end
    set(gca, 'Xtick', tick);
    set(gca, 'Xticklabel', tick_label);
    setappdata(0, 'tt1', t1);
    setappdata(0, 'tt2', t2);
    setappdata(0, 'line_end', line_end);
    axis xy
    ylabel('Frequency(kHz)')
    caxis([0 getappdata(0, 'intensity')])
    colormap(color);
    hold off
    ylim(getappdata(0,'freq_range'))
    box off
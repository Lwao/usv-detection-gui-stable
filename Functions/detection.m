function [elapsed_time, time_start_index, time_end_index, time_threshold, zeroed_entropy, rms_time_entropy, USV_data] = detection(varargin)
    tic
    % variables
    initial_frequency = varargin{1};
    final_frequency = varargin{2};
    time_threshold_constant = varargin{3};
    frequency_threshold_constant = varargin{4};
    min_interval_to_distinct_usv = varargin{5};
    min_duration_to_consider_usv = varargin{6};
    spectog = varargin{7};
    step = varargin{8};
    frequency = varargin{9};
    select = varargin{10};
    
    wait_bar = waitbar(0,'Detecting...');
    
    % select spectrogram range to work on
    indx_freq_range = find(frequency >= initial_frequency & frequency <= final_frequency);
    S_probab = bsxfun(@rdivide,spectog(:,indx_freq_range),sum(spectog(:,indx_freq_range),2));

    %%% Time processing

    % time entropy
    if(select==0) % Wiener
        time_entropy = geomean(S_probab')./mean(S_probab,2)'; % Wiener Entropy
        time_entropy = smooth(zscore(time_entropy));
        rms_time_entropy = rms(time_entropy);
        zeroed_entropy = zscore(time_entropy,1,1);
        time_threshold = (-1)*time_threshold_constant/(std(time_entropy));  % entropy threshold
        cross_thold = (zeroed_entropy <= time_threshold);
    else % Shannon
        time_entropy = -sum(S_probab.*log2(S_probab),2); % Shannon Entropy
        rms_time_entropy = rms(time_entropy);
        zeroed_entropy = detrend((time_entropy-rms_time_entropy)/std(time_entropy),'constant');
        time_threshold= (-1)*time_threshold_constant/(std(time_entropy));  % entropy threshold
        cross_thold = (zeroed_entropy <= time_threshold);
    end
    clear S_probab 

    nousv_flag = false;
    if(isempty(cross_thold))
        nousv_flag = true;
        warning('No USVs detected. Please change your parameters.');
        return
    end

    % correction to apply USV onset identifier when it starts before the recording
    if (cross_thold(1) == 1 && cross_thold(2) == 0) cross_thold(2) = 1; end
    cross_thold(1)=0;
    cross_thold(end) = 0;

    diff_cross_thold = diff(cross_thold); % 1:begin of usv, -1:end of usv
    x_zeros_ini = find(diff_cross_thold==1)+1; % index after correction of diff
    x_zeros_end = find(diff_cross_thold==-1)+1; % index after correction of diff
    dVec = x_zeros_ini(2:end) - x_zeros_end(1:end-1); % time interval between vocalizations

    % concatenation of too close vocalizations 
    time_start_index = x_zeros_ini(1:length(x_zeros_end));
    time_end_index = x_zeros_end;

    % evaluate minimum interval to accept distinct vocalizations (ms)
    minInt_index = dVec <= min_interval_to_distinct_usv;
    time_start_index([false;minInt_index]) = [];
    time_end_index(minInt_index) = [];

    dur = (time_end_index-time_start_index)*step;
    dur_index = dur >= min_duration_to_consider_usv/1000;
    time_start_index = time_start_index(dur_index);
    time_end_index = time_end_index(dur_index);
    dur_filt = dur(dur_index);

    clear cross_thold dur dVec i 

    %%% Frequency processing

    %%% Entropy in Frequency 
    lenTime = length(time_start_index);
    S_probab_f_cell = cell(lenTime,1);
    len_S = length(spectog(:,:));

    time_start_index(time_start_index-5<0) = 1;
    time_end_index(time_end_index+5>len_S) = len_S;

    i = 0;
    for i = 1:lenTime
        waitbar(i/(2*lenTime), wait_bar);
        S_probab_f_cell{i,1} = bsxfun(@rdivide,...
            double(spectog(time_start_index(i)-5:time_end_index(i)+5,:)),...
            sum(double(spectog(time_start_index(i)-5:time_end_index(i)+5,:)),1));
    end

    if(select==0) % Wiener
        A = cellfun(@(x) geomean(x),S_probab_f_cell,'UniformOutput', false);
        B = cellfun(@(x) mean(x),S_probab_f_cell,'UniformOutput', false);
        freq_entropy = cellfun(@rdivide,A,B,'UniformOutput', false); % Wiener Entropy
        clearvars A B
    else % Shannon
        A = cellfun(@(x) (x.*log2(x)),S_probab_f_cell,'UniformOutput', false);
        freq_entropy = cellfun(@(x) (-1)*sum(x,1),A,'UniformOutput', false); % Shannon Entropy
        clearvars A 
    end
    clear S_probab_f_cell

    frequency_entropy = cell2mat(freq_entropy);
    rms_frequency_entropy = rms(frequency_entropy,2);
    zeroed_entropy_f = zscore(frequency_entropy,1,2)';
    frequency_threshold = (-1)*frequency_threshold_constant./(std(frequency_entropy,0,2));
    cross_thold_f = zeroed_entropy_f <= repmat(frequency_threshold',size(zeroed_entropy_f,1),1);

    diff_cross_thold_f = diff(cross_thold_f',1,2);

    [uprise_row,uprise_column]= find(diff_cross_thold_f==1);
    [downrise_row,downrise_column]= find(diff_cross_thold_f==-1);

    uprise = sortrows([uprise_row uprise_column]);
    downrise = sortrows([downrise_row downrise_column]);

    frequency_start_index = ones(lenTime,1);
    frequency_end_index = length(frequency)*ones(lenTime,1);

    for  j = 1:lenTime
        waitbar((i+j)/(2*lenTime), wait_bar);
        idxCandidates2start = find(uprise(:,1) == j);        
        if (~isempty(idxCandidates2start)), frequency_start_index(j,1) = 1+ min(uprise(idxCandidates2start,2)); end % extract idx with minimal frequency

        idxCandidates2end = find(downrise(:,1) == j);        
        if (~isempty(idxCandidates2end)), frequency_end_index(j,1) = 1+max(downrise(idxCandidates2end,2)); end % extract idx with maximal frequency
    end
    clear idxCandidates2start idxCandidates2end uprise downrise cross_thold_f diff_cross_thold_f

    freq_Min = frequency(frequency_start_index)'/1000;
    freq_Range = frequency(frequency_end_index)'/1000 - freq_Min;
    
    %%% Set data
    
    USV_times = [time_start_index*step,time_end_index*step,dur_filt];
    USV_data = [USV_times(:,1) freq_Min USV_times(:,3) freq_Range];

    elapsed_time = toc;
    close(wait_bar)

    clear spectog
end
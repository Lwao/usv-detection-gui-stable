function [NEW_k, NEW_curated_usvs, NEW_window_Begin, NEW_window_Dur] = new_detection(spacer, step, NEW_k, h, NEW_window_Begin, NEW_window_Dur, NEW_curated_usvs, k, t, t1, t2)
    n = length(NEW_curated_usvs(:,1));
    NEW_curated_usvs(n,1) = n;
    NEW_curated_usvs(n,[2:5]) = getPosition(h);
    if (n==1)
        NEW_window_Begin(1) = NEW_curated_usvs(1,2) - spacer;
        NEW_window_Dur(1) = NEW_curated_usvs(1,2) + NEW_curated_usvs(1,4) + spacer - NEW_window_Begin(1);
        NEW_k(1) = k;
    else
        NEW_window_Begin = [NEW_window_Begin; (NEW_curated_usvs(n,2) - spacer)];
        NEW_window_Dur = [NEW_window_Dur; (NEW_curated_usvs(n,2) + NEW_curated_usvs(n,4) + spacer - NEW_window_Begin(n))];
        NEW_k = [NEW_k k];
    end
    NEW_curated_usvs(n,2) = NEW_curated_usvs(n,2) - NEW_window_Begin(n);
    NEW_curated_usvs(n,6) = 1;
    NEW_curated_usvs = [NEW_curated_usvs; 0, 0, 0, 0, 0, 0];
end
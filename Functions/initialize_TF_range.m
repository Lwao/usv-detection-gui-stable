function handles = initialize_TF_range(pop_choice, handles)
if(pop_choice ~= ' ')
    set(handles.pushbutton_confirmf,'Enable','on')
    set(handles.pushbutton_confirmt,'Enable','on')
    
    % Commom variables
    
    audio_pathname = getappdata(0, 'audio_pathname');
    audio_name = getappdata(0, 'name');

    [audio_data,fs] = audioread([audio_pathname audio_name],'native');
    max_time = length(audio_data)/fs;
    max_freq = fs/2/1000; % half sampling frequency [kHz]
    
    clearvars audio_data fs audio_name audio_pathname
    
    % Time
    
    if(isempty(getappdata(0,'time_range')))
        handles.initial_time = 0;
        handles.final_time = max_time;
    else
        temp = getappdata(0,'time_range');
        handles.initial_time = temp(1);
        handles.final_time = temp(2);        
        clearvars temp
    end
    
    set(handles.edit_tstart, 'String', num2str(handles.initial_time));
    set(handles.edit_tend, 'String', num2str(handles.final_time));
    set(handles.text_totaltime, 'String', strcat('Max. time (s): ', num2str(max_time)));
    
    setappdata(0,'time_range',[handles.initial_time, handles.final_time])
    setappdata(0,'max_time', max_time);
    
    clearvars max_time
    
    % Frequency
    
    if(isempty(getappdata(0,'freq_range')))
        handles.initial_freq = 0;
        handles.final_freq = max_freq;
    else
        temp = getappdata(0,'freq_range');
        handles.initial_freq = temp(1);
        handles.final_freq = temp(2);        
        clearvars temp
    end
    
    set(handles.edit_fstart, 'String', num2str(handles.initial_freq));
    set(handles.edit_fend, 'String', num2str(handles.final_freq));
    set(handles.text_totalfrequency, 'String', strcat('Max. frequency (kHz): ', num2str(max_freq)));
    
    setappdata(0,'freq_range',[handles.initial_freq, handles.final_freq])
    setappdata(0,'max_freq', max_freq);
    
    clearvars max_freq
end

end
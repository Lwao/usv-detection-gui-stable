function set_spec(S, f, t, cint, cmap, freq_range, axes2)
    to = 0;
    axes(axes2)
    imagesc(t+to,f/1000,S');
    axis xy; 
    xlabel('Time (s)')
    ylabel('Frequency (kHz)')
    caxis([0 cint])
    ylim(freq_range)
    colormap(cmap)
    box off
end
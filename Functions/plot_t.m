function plot_t(A_temp, n, t, t1, t2, audio_data, range, Amin, Amax)
    if(n~=1)
       hold on 
    end
    dur_i = 1;
    dur_f = 0;
    line_end = [];
    for i = 1:n
        dur_f = dur_f+length(t(t1(i):t2(i)));
        if(range)
            x_len = length(dur_i:1/round(length(audio_data)/length(t)):dur_f);
            y_len = length([A_temp{i}]);
            if(x_len>y_len)
                inc = x_len-y_len;
                temp = zeros(inc, 1);
                A = [[A_temp{i}];temp];
                plot(dur_i:1/round(length(audio_data)/length(t)):dur_f,A,'k')
            else
                plot(dur_i:1/round(length(audio_data)/length(t)):dur_f,[A_temp{i}],'k')
            end
            %plot(dur_i:1/round(length(audio_data)/length(t)):dur_f,A,'k')
        else
            plot(dur_i:1/round(length(audio_data)/length(t)):dur_f,[A_temp{i}],'k')
        end
        line_end(i) = dur_f;
        dur_i = dur_f+1;
    end
    % linhas
    for i = 1:length(line_end)
        line([line_end(i) line_end(i)],[Amin Amax],'Color',[0 1 0],'LineStyle','--'); 
    end
    % labels
    tick = [];
    tick_label = [];
    div = 4;
    for i = 1:n
        if(i==1)
            step = line_end(i)/div;
            tick = [tick 0:step:line_end(i)];
        else
            step = (line_end(i)-line_end(i-1))/div;
            tick = [tick line_end(i-1)+step:step:line_end(i)];
        end
        dur = t(t2(i))-t(t1(i));
        tick_step = round(dur/div,2);
        if(i==1)
            tick_label = [tick_label round(t(t1(i)),2):tick_step:round(t(t2(i)),2)];
        else
            tick_label = [tick_label(1:end-1) round(t(t1(i)),2):tick_step:round(t(t2(i)),2)];
        end 
        if(length(tick_label)<length(tick))
            tick_label = [tick_label tick_label(end)+tick_label(2)-tick_label(1)];
        end
    end
    set(gca, 'Xtick', tick);
    set(gca, 'Xticklabel', tick_label);
    
    box off
    axis xy
    xlabel('Time (s)')
    ylabel('Amplitude')
    hold off
end
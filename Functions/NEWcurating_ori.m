function [h_m, h, t1, t2, window_Begin, window_Dur] = NEWcurating(curated_usvs, window_Begin, window_Dur, col, h_m, name, color_select, k, spacer, intensity, step, S, t, f, axes1, axes2)
    cla(axes1);
    cla(axes2);

    n = length(window_Begin);
    temp = round(t, 4);
    for i = 1:n
        temp1 = window_Begin(i);
        temp2 = temp1+window_Dur(i);
        if(i==1)
            t1 = find((temp<temp1+0.001)&(temp>temp1-0.001));
            t2 = find((temp<temp2+0.001)&(temp>temp2-0.001));
        else
            t1 = [t1 find((temp<temp1+0.001)&(temp>temp1-0.001))];
            t2 = [t2 find((temp<temp2+0.001)&(temp>temp2-0.001))];
        end
        window_Begin(i) = t(t1(i));
        window_Dur(i) = t(t2(i))-t(t1(i));
    end

    a = find(t2 > length(S));
    t2(a) = length(S);

    b = find(t1 < 1);
    t1(b) = 1;

    S_temp1 = S(t1(k):t2(k),:)';
    if k < length(window_Begin)-2
        S_temp2 = S(t1(k):t2(k+2),:)';
    else
        S_temp2 = S(t1(k):t2(end),:)';
    end

    if t2(k) > length(S)
        t2(k) = length(S);
        S_temp1 = S(t1(k):t2(k),:)';    
    end
        
    axes(axes1)
        
    % plot spectogram

    imagesc(t(t1(k):t2(k)),f/1000,S_temp1);
    title(getappdata(0, 'name'));
    hold on
    % Localizando se existe uma posi��o anterior para o ret�ngulo
    if (all(h_m(k, :)==0))
        h=imrect(axes1,[curated_usvs(k, 2)+window_Begin(k), curated_usvs(k, 3:5)]);
        h_m(k, :) = getPosition(h);
    else
        h=imrect(axes1,h_m(k,:));
    end
    setColor(h, col(k, :));

    box off
    axis xy
    ylabel('Frequency(kHz)')
    caxis([0 intensity])
    colormap(color_select);
    hold off
    ylim([20 125])

    axes(axes2)
    % plot next USVs
    
    if k < length(window_Begin)-2  % antes das tr�s �ltimas
        imagesc(t(t1(k):t2(k+2)),f/1000,S_temp2);
        hold on
        %atual
        if (all(h_m(k, :)==0))
            line([t(t1(k)+spacer/step) t(t1(k)+spacer/step)],[20 125],'Color',col(k, :),'LineStyle','-')
            line([t(t2(k)-spacer/step) t(t2(k)-spacer/step)],[20 125],'Color',col(k, :),'LineStyle','-')
        else
            line([h_m(k, 1) h_m(k, 1)],[20 125],'Color',col(k, :),'LineStyle','-')
            line([h_m(k, 3)+h_m(k, 1) h_m(k, 3)+h_m(k, 1)],[20 125],'Color',col(k, :),'LineStyle','-')
        end
        % 1� pr�xima
        if (all(h_m(k+1, :)==0))
            line([t(t1(k+1)+spacer/step) t(t1(k+1)+spacer/step)],[20 125],'Color',col(k+1, :),'LineStyle','--')
            line([t(t2(k+1)-spacer/step) t(t2(k+1)-spacer/step)],[20 125],'Color',col(k+1, :),'LineStyle','--')
        else
            line([h_m(k+1, 1) h_m(k+1, 1)],[20 125],'Color',col(k+1, :),'LineStyle','--')
            line([h_m(k+1, 3)+h_m(k+1, 1) h_m(k+1, 3)+h_m(k+1, 1)],[20 125],'Color',col(k+1, :),'LineStyle','--')
        end
        % 2� pr�xima
        if (all(h_m(k+2, :)==0))
            line([t(t1(k+2)+spacer/step) t(t1(k+2)+spacer/step)],[20 125],'Color',col(k+2, :),'LineStyle','--')
            line([t(t2(k+2)-spacer/step) t(t2(k+2)-spacer/step)],[20 125],'Color',col(k+2, :),'LineStyle','--')
        else
            line([h_m(k+2, 1) h_m(k+2, 1)],[20 125],'Color',col(k+2, :),'LineStyle','--')
            line([h_m(k+2, 3)+h_m(k+2, 1) h_m(k+2, 3)+h_m(k+2, 1)],[20 125],'Color',col(k+2, :),'LineStyle','--')
        end
        box off
        axis xy
        xlabel('Time (s)')
        ylabel('Frequency(kHz)')
        caxis([0 intensity])
        colormap(color_select);
        hold off
        ylim([20 125])
    else % depois das tr�s �ltimas
        imagesc(t(t1(k):t2(end)),f/1000,S_temp2);
        hold on
        % atual
        if (all(h_m(k, :)==0))
            line([t(t1(k)+spacer/step) t(t1(k)+spacer/step)],[20 125],'Color',col(k, :),'LineStyle','-')
            line([t(t2(k)-spacer/step) t(t2(k)-spacer/step)],[20 125],'Color',col(k, :),'LineStyle','-')
        else
            line([h_m(k, 1) h_m(k, 1)],[20 125],'Color',col(k, :),'LineStyle','-')
            line([h_m(k, 3)+h_m(k, 1) h_m(k, 3)+h_m(k, 1)],[20 125],'Color',col(k, :),'LineStyle','-')
        end
        %final
        if (all(h_m(end, :)==0))
            line([t(t1(end)+spacer/step) t(t1(end)+spacer/step)],[20 125],'Color',col(end, :),'LineStyle','--')
            line([t(t2(end)-spacer/step) t(t2(end)-spacer/step)],[20 125],'Color',col(end, :),'LineStyle','--')
        else
            line([h_m(end, 1) h_m(end, 1)],[20 125],'Color',col(end, :),'LineStyle','--')
            line([h_m(end, 3)+h_m(end, 1) h_m(end, 3)+h_m(end, 1)],[20 125],'Color',col(end, :),'LineStyle','--')
        end
        box off
        axis xy
        xlabel('Time (ms)')
        ylabel('Frequency(kHz)')
        caxis([0 intensity])
        colormap(color_select);
        hold off
        ylim([20 125])
    end
end
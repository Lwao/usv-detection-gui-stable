function save_calls(NEW_curated_usvs, NEW_window_Begin, NEW_window_Dur, name, curated_usvs, window_Begin, window_Dur, step, win, params, thre, dur_min, isi, f_ini, f_end, fs, audio_data)
    NEW_curated_usvs(end,:) = [];
    B = [curated_usvs, window_Dur, window_Begin; NEW_curated_usvs, NEW_window_Dur, NEW_window_Begin];
    [~,inx]=sort(B(:,end));
    out = B(inx,:);
    temp = [];
    for i = 1:length(out(:, 1))
       if(~(out(i, 6)==0))
          temp = [temp; out(i, :)]; 
       end
    end
    n = length(temp(:,1));
    curated_usvs = temp(1:n, 1:6);
    window_Begin = temp(1:n, 8);
    window_Dur = temp(1:n, 7);
    for i = 1:length(window_Begin)
       if(window_Begin(i)<0)
          window_Begin(i) = 0.005; 
       end
    end
%     assignin('base', 'curated_usvs', curated_usvs);
%     assignin('base', 'window_Begin', window_Begin);
%     assignin('base', 'window_Dur', window_Dur);
    
 
    try
        [file,path,indx] = uiputfile([name(1:end-4) '.mat']);

        window_Begin(window_Begin==0) = [];
        window_Dur(window_Dur==0) = [];
        curated_usvs(curated_usvs(:,2)==0,:) = [];


        data_struct.file_name = name;
        data_struct.spectrogram_parameters.step = step; %in seconds
        data_struct.spectrogram_parameters.win = win; %in seconds
        data_struct.spectrogram_parameters.params = params;
        data_struct.USVdetection.entropy_threshold = thre; % A.U.
        data_struct.USVdetection.min_dur = dur_min; % in ms
        data_struct.USVdetection.min_interval = isi; %in ms
        data_struct.USVdetection.entropy_freq_interval = [f_ini,f_end]; %in hertz

%         for i = 1:size(curated_usvs,1)
        for i = 1:length(curated_usvs(:,1));
            Calls(i).Rate = fs;
            Calls(i).Box = [window_Begin(i) curated_usvs(i,3) window_Dur(i) curated_usvs(i,5)]; %in seconds
            Calls(i).RelBox = [curated_usvs(i,[2:5])];
            Calls(i).Score = 0;
            Calls(i).Audio = audio_data(window_Begin(i)*fs : window_Begin(i)*fs + window_Dur(i)*fs); %in seconds
            Calls(i).Type = categorical({'USV'});
            Calls(i).Power = 0;
            Calls(i).Accept = 1;
        end
        

%         for i = 1:size(curated_usvs,1)
%             data_struct.Calls(i).Rate = fs;
%             data_struct.Calls(i).Box = [window_Begin(i) curated_usvs(i,3) window_Dur(i) curated_usvs(i,5)]; %in seconds
%             data_struct.Calls(i).RelBox = [curated_usvs(i,[2:5])];
%             data_struct.Calls(i).Score = 0;
%             data_struct.Calls(i).Audio = audio_data(window_Begin(i)*fs : window_Begin(i)*fs + window_Dur(i)*fs); %in seconds
%             data_struct.Calls(i).Type = categorical({'USV'});
%             data_struct.Calls(i).Power = 0;
%             data_struct.Calls(i).Accept = 1;
%         end
        
        %Calls = data_struct.Calls;
        save([path file],'Calls', 'data_struct');
    end
end
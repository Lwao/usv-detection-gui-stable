 function [h_m, h, t1, t2] = curating(NEW_k, NEW_cusv, NEW_window_Begin, col, h_m, name, color_select, k, spacer, intensity, idx_ini, idx_end, step, S, t, thre, Entropy, rms_entropy, f, USV_data, freq_range, axes1, axes2, axes3)
    cla(axes1);
    cla(axes2);
    cla(axes3);
    
    t1 = idx_ini - spacer/step;
    t2 = idx_end + spacer/step;

    a = find(t2 > length(S));
    t2(a) = length(S);

    b = find(t1 < 1);
    t1(b) = 1;

    S_temp1 = S(t1(k):t2(k),:)';
    if k < length(idx_ini)-2
        S_temp2 = S(t1(k):t2(k+2),:)';
    else
        S_temp2 = S(t1(k):t2(end),:)';
    end

    if t2(k) > length(S)
        t2(k) = length(S);
        S_temp1 = S(t1(k):t2(k),:)';    
    end
        
    axes(axes1)
        
    % plot spectogram

    imagesc(t(t1(k):t2(k)),f/1000,S_temp1);
    title(getappdata(0, 'name'));
    hold on
    % Localizando se existe uma posi��o anterior para o ret�ngulo
    if (all(h_m(k, :)==0))
        h=imrect(axes1,USV_data(k,:));
        h_m(k, :) = getPosition(h);
    else
        h=imrect(axes1,h_m(k,:));
    end
    setColor(h, col(k, :));

    box off
    axis xy
    ylabel('Frequency(kHz)')
    caxis([0 intensity])
    colormap(color_select);
    hold off
    ylim(freq_range)

    axes(axes2)
        
    % plot entropy
    plot(t(t1(k):t2(k)),Entropy(t1(k):t2(k)),'k');
    box off
    hold on
    line([t(t1(k)),t(t2(k))],[thre,thre],'Color','g','LineStyle','--');
    ylabel('Norm. Entropy')

    % plot timestamps of start and end of vocalizations
    scatter(t(t1(k)+spacer/step),zeros(1,1),20,'^','MarkerEdgeColor',[1 0 0],...
                   'MarkerFaceColor',[1 0 0])
    scatter(t(t2(k)-spacer/step),zeros(1,1),20,'v','MarkerEdgeColor',[0 0 1],...
                   'MarkerFaceColor',[0 0 1])
    linkaxes([axes1,axes2],'x')

    axes(axes3)
    % plot next USVs
    
        if k < length(idx_ini)-2  % antes das tr�s �ltimas
            imagesc(t(t1(k):t2(k+2)),f/1000,S_temp2);
            hold on
            %atual
             if (all(h_m(k, :)==0))
                 line([t(t1(k)+spacer/step) t(t1(k)+spacer/step)],[20 120],'Color',col(k, :),'LineStyle','-')
                 line([t(t2(k)-spacer/step) t(t2(k)-spacer/step)],[20 120],'Color',col(k, :),'LineStyle','-')
             else
                 line([h_m(k, 1) h_m(k, 1)],[20 120],'Color',col(k, :),'LineStyle','-')
                 line([h_m(k, 3)+h_m(k, 1) h_m(k, 3)+h_m(k, 1)],[20 120],'Color',col(k, :),'LineStyle','-')
             end
            % 1� pr�xima
            if (all(h_m(k+1, :)==0))
                line([t(t1(k+1)+spacer/step) t(t1(k+1)+spacer/step)],[20 120],'Color',col(k+1, :),'LineStyle','--')
                line([t(t2(k+1)-spacer/step) t(t2(k+1)-spacer/step)],[20 120],'Color',col(k+1, :),'LineStyle','--') 
            else
                line([h_m(k+1, 1) h_m(k+1, 1)],[20 120],'Color',col(k+1, :),'LineStyle','--')
                line([h_m(k+1, 3)+h_m(k+1, 1) h_m(k+1, 3)+h_m(k+1, 1)],[20 120],'Color',col(k+1, :),'LineStyle','--')
            end
            % 2� pr�xima
            if (all(h_m(k+2, :)==0))
                line([t(t1(k+2)+spacer/step) t(t1(k+2)+spacer/step)],[20 120],'Color',col(k+2, :),'LineStyle','--')
                line([t(t2(k+2)-spacer/step) t(t2(k+2)-spacer/step)],[20 120],'Color',col(k+2, :),'LineStyle','--')
            else
                line([h_m(k+2, 1) h_m(k+2, 1)],[20 120],'Color',col(k+2, :),'LineStyle','--')
                line([h_m(k+2, 3)+h_m(k+2, 1) h_m(k+2, 3)+h_m(k+2, 1)],[20 120],'Color',col(k+2, :),'LineStyle','--') 
            end
            box off
            axis xy
            xlabel('Time (s)')
            ylabel('Frequency(kHz)')
            caxis([0 intensity])
            colormap(color_select);
            hold off
            ylim(freq_range)
        else % depois das tr�s �ltimas
            imagesc(t(t1(k):t2(end)),f/1000,S_temp2);
            hold on
            % atual
             if (all(h_m(k, :)==0))
                 line([t(t1(k)+spacer/step) t(t1(k)+spacer/step)],[20 125],'Color',col(k, :),'LineStyle','-')
                 line([t(t2(k)-spacer/step) t(t2(k)-spacer/step)],[20 125],'Color',col(k, :),'LineStyle','-')
             else
                 line([h_m(k, 1) h_m(k, 1)],[20 125],'Color',col(k, :),'LineStyle','-')
                 line([h_m(k, 3)+h_m(k, 1) h_m(k, 3)+h_m(k, 1)],[20 125],'Color',col(k, :),'LineStyle','-')
             end
            %final
            if (all(h_m(end, :)==0))
                line([t(t1(end)+spacer/step) t(t1(end)+spacer/step)],[20 125],'Color',col(end, :),'LineStyle','--')
                line([t(t2(end)-spacer/step) t(t2(end)-spacer/step)],[20 125],'Color',col(end, :),'LineStyle','--') 
            else
                line([h_m(end, 1) h_m(end, 1)],[20 125],'Color',col(end, :),'LineStyle','--')
                line([h_m(end, 3)+h_m(end, 1) h_m(end, 3)+h_m(end, 1)],[20 125],'Color',col(end, :),'LineStyle','--') 
            end
            box off
            axis xy
            xlabel('Time (ms)')
            ylabel('Frequency(kHz)')
            caxis([0 intensity])
            colormap(color_select);
            hold off
            ylim(freq_range)
        end    
        % linhas para as novas usvs
        for i = 1:length(NEW_k)
            if(NEW_k(i)==k)
                temp = NEW_cusv(i, 2)+NEW_window_Begin(i);
                line([temp temp],[20 120],'Color',[0 0.6 0.8],'LineStyle','--')
                line([temp+NEW_cusv(i, 4) temp+NEW_cusv(i, 4)],[20 125],'Color',[0 0.6 0.8],'LineStyle','--') 
            end            
        end   
end
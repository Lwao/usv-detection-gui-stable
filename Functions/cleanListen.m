
function cleanListen(obj, intervals)
% set(gca, 'Xtick', [0 25 50 75 100]);
% set(gca, 'Xticklabel', [0 25 50 75 100]);
x = get(gca,'xlim');
xmin = round(x(1));
xmax = round(x(2))-1;
div = 4;
step = (x(2)-x(1))/div;
xtick = x(1):step:x(2);
dur = intervals(xmax)-intervals(xmin);
tick_step = round(dur/div,2);
xlabel = round(intervals(xmin),2):tick_step:round(intervals(xmax),2);

for i = 1:length(obj)
    set(obj(i), 'Xtick', xtick);
    set(obj(i), 'Xticklabel', xlabel);
end
% obj.listenerCallback();
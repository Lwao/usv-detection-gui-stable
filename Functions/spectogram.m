function [elapsed, S, t, f, params, fs, audio_data, y] = spectogram(audio_pathname, audio_name, win, step, time_range, freq_range)
    wait_bar = waitbar(0,'Loading...');

    [audio_data,~] = audioread([audio_pathname audio_name],'native');
    waitbar(1/10, wait_bar);
    [y,fs] = audioread([audio_pathname audio_name],'double');
    waitbar(2/10, wait_bar);

    if(isempty(time_range)) 
        setappdata(0,'time_range', [0 length(y)/fs]); 
        time_range = getappdata(0,'time_range');
    end
    if(isempty(freq_range)) 
        setappdata(0,'freq_range', [18000 fs/2]*1e-3); 
        freq_range = getappdata(0,'freq_range');
    end
        
    to = time_range(1);
    tf = time_range(2);
    params.tapers = [7 11];
    params.Fs = fs;
    params.pad = 0;
    params.fpass = [freq_range(1) freq_range(2)]*1e3;
    params.err = 0;
    params.trialave = 0;
    params.err = [0 0.05];
   
    waitbar(4/10, wait_bar);
   
   
    tic

    waitbar(6/10, wait_bar);
    
    [S,t,f]=mtspecgramc(y(1+to*params.Fs:tf*params.Fs,1),[win step],params);
    elapsed = toc;
    
    waitbar(8/10, wait_bar);
    waitbar(1, wait_bar);
    close(wait_bar);
end
function [elapsed, idx_ini, idx_end, S, thre, Entropy, rms_entropy, USV_data] = detection(f_ini, f_end, thre_const, thre_freq, isi, dur_min, S, step, f, select)
    wait_bar = waitbar(0,'Detecting...');
    tic
    indx_freq_range = find(f >= f_ini & f <= f_end);
        
    S_probab = bsxfun(@rdivide,S(:,indx_freq_range),sum(S(:,indx_freq_range),2));
        
    %assignin('base', 'spec', S)
    
    if(select==0)
        Entropy = geomean(S_probab')./mean(S_probab,2)'; % Wiener Entropy
        Entropy = smooth(zscore(Entropy));
        rms_entropy=rms(Entropy);
        zeroed_entropy = zscore(Entropy);
        thre= (-1)*thre_const/(std(Entropy));  % entropy threshold
        cross_thold = (zeroed_entropy <= thre);
    else
        Entropy = -sum(S_probab.*log2(S_probab),2); % Shannon Entropy
        Entropy = smooth(zscore(Entropy));
        rms_entropy=rms(Entropy);
        %rms_entropy=mean(Entropy);
        zeroed_entropy = detrend((Entropy-rms_entropy)/std(Entropy),'constant');
        thre= (-1)*thre_const/(std(Entropy));  % entropy threshold
        cross_thold = (zeroed_entropy <= thre);
    end
    
   
    % #############################################################
    if cross_thold(1) == 1 && cross_thold(2) == 0   % correction to apply USV onset identifier when
        cross_thold(2) = 1;                         % it starts before the recording
    end
    cross_thold(1)= 0;
    cross_thold(end) = 0;
    % #############################################################

    b = diff(cross_thold);
    c1 = find(b==1);  % onset
    c2 = find(b==-1); % offset
    
    disp(['Onset-Offset:'])
    x_zeros_ini = c1 + 1; % index after correction of diff       
    x_zeros_end = c2 + 1; % index after correction of diff           
    [x_zeros_ini x_zeros_end]* step
    
    dVec = zeros(length(x_zeros_ini)-1,1);
    % time interval between vocalizations
    for i = 1:length(x_zeros_ini)-1
        dVec(i,1) = x_zeros_ini(i+1) - x_zeros_end(i);
    end

    % concatenation of too close vocalizations 

    idx_ini = x_zeros_ini(1:length(x_zeros_end));
    idx_end = x_zeros_end;
    
    for i = 1:length(dVec)
        if dVec(i) <= isi  % minimum interval to accept distinct vocalizations (ms)
            idx_ini(i+1) = 0;
            idx_end(i) = 0;           
        end
    end

    % delete zeroed timestamps
    idx_ini(idx_ini == 0)=[];
    idx_end(idx_end == 0)=[];

    dur = idx_end*step - idx_ini*step;
    idx_ini = idx_ini(dur >= dur_min/1000);
    idx_end = idx_end(dur >= dur_min/1000);
    dur_filt = dur(dur >= dur_min/1000);

    clear cross_thold b c1 c2 dur dVec i 

    %%% Entropy in Frequency 

    rms_entropy_f = zeros(length(idx_ini),1);
    %zeroed_entropy_f = zeros(:, length(idx_ini));
    thre_f = zeros(length(idx_ini),1);
    idx_ini_f = zeros(length(idx_ini),1);
    idx_end_f = zeros(length(idx_ini),1);
    for i = 1:length(idx_ini)
        waitbar(i/length(idx_ini), wait_bar);
        if idx_end(i)+5 > length(S)
            S_usv = S(idx_ini(i)-5:end,indx_freq_range);
            S_probab_f = bsxfun(@rdivide,S(idx_ini(i)-5:end,indx_freq_range),...
            sum(S(idx_ini(i)-5:end,indx_freq_range),1));        
        elseif idx_ini(i)-5 < 0 
            S_usv = S(1:idx_end(i)+5,indx_freq_range);
            S_probab_f = bsxfun(@rdivide,S(1:idx_end(i)+5,indx_freq_range),...
            sum(S(1:idx_end(i)+5,indx_freq_range),1));        
        else
            S_usv = S(idx_ini(i)-5:idx_end(i)+5,indx_freq_range);
            S_probab_f = bsxfun(@rdivide,S(idx_ini(i)-5:idx_end(i)+5,indx_freq_range),...
            sum(S(idx_ini(i)-5:idx_end(i)+5,indx_freq_range),1));
        end
        select = 0;
        if(select==0)
            Entropy_f = geomean(S_probab_f)./mean(S_probab_f,1); % Wiener Entropy
        else
            Entropy_f = -sum(S_probab_f.*log2(S_probab_f),1); % Shannon Entropy 
        end
        Entropy_f = smooth(Entropy_f);
        size(Entropy_f)
        
        Powfreq = sum(S_usv,1)';
        Powfreq = Powfreq./max(Powfreq);
        size(Powfreq)
        min(Powfreq)
        max(Powfreq)
        Entropy_f = Entropy_f.*(Powfreq.^(1/4));
        
        %temp1 = rms(Entropy_f);
        temp1 = mean(Entropy_f);
        rms_entropy_f(i,1) = temp1;
                
        temp2 = (Entropy_f-rms_entropy_f(i,1))/std(Entropy_f);
        zeroed_entropy_f(:,i) = temp2;
                
        temp3 = (-1)*thre_freq/(std(Entropy_f));
        thre_f(i,1) = temp3;

        cross_thold_f = (zeroed_entropy_f(:,i) <= thre_f(i));        
        a = diff(cross_thold_f);
        a1 = find(a==1);
        a2 = find(a==-1);

        temp41 = a1 + 1;
        if isempty(temp41)
            idx_ini_f(i,1) = 1;
        else
            idx_ini_f(i,1) = min(temp41);
        end

        temp42 = a2 + 1;
        if isempty(temp42)
            idx_end_f(i,1) = find(f >= 100000,1);
        else
            idx_end_f(i,1) = max(temp42);
        end


        
        
        
        clear a a1 a2 cross_thold_f temp1 temp2 temp3 temp41 temp42
    end

    clear i Entropy_f S_probab_f

    freq_Min = f(idx_ini_f)'/1000;
    freq_Range = f(idx_end_f)'/1000 - freq_Min;
    
    usv_times = [idx_ini*step,idx_end*step,dur_filt];
    USV_data = [usv_times(:,1) freq_Min usv_times(:,3) freq_Range];
    elapsed = toc;
    close(wait_bar)
end

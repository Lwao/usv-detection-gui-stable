function [audio_fname, audio_pathname] = load_sound_file()
    [audio_fname, audio_pathname, filter] = uigetfile({'*.wav','DATA WAV-files (*.wav)'},'MultiSelect','on');
    if filter == 0
        audio_fname = ' ';
        audio_pathname = 'cd/';
    else
        audio_fname = audio_fname';
        if ~iscell(audio_fname)
            audio_fname = {audio_fname'};    
        end
    end
end


function [curated_usvs, window_Begin, window_Dur, h] = redefine(h, curated_usvs, k, t1, t2, t, axes1)
    delete(h)
    h=imrect(axes1);
    window_Begin(k,1) = t(t1(k));
    window_Dur(k,1) = t(t2(k))-t(t1(k));
    curated_usvs(k,1) = k;
    %try
        curated_usvs(k,[2:5]) = getPosition(h);
    %catch
     %   curated_usvs(k,[2:5]) = [0 0 0 0];    
    %end
    curated_usvs(k,2) = curated_usvs(k,2) - window_Begin(k);
end
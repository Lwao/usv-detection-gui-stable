function save_session(name, k, curated_usvs, window_Begin, window_Dur, NEW_curated_usvs, NEW_window_Begin, NEW_window_Dur, matrix_h, color_h, NEW_k, NEW_matrix_h, NEW_color_h)
    try
        name = name(1:end-4); % remove .wav
        [file,path,indx] = uiputfile(['Session_' name '.mat']);
        %k = k-1;
        save([path file],'curated_usvs','window_Begin','window_Dur','k', 'NEW_curated_usvs', 'NEW_window_Begin', 'NEW_window_Dur', 'NEW_k', 'matrix_h', 'color_h', 'NEW_matrix_h', 'NEW_color_h')
    end
end
    

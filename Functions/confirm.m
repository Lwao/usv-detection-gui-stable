function [curated_usvs, window_Begin, window_Dur] = confirm(window_Begin, window_Dur, curated_usvs, k, t, t1, t2, h)
    window_Begin(k,1) = t(t1(k));
    window_Dur(k,1) = t(t2(k))-t(t1(k));
    curated_usvs(k,1) = k;
    curated_usvs(k,[2:5]) = getPosition(h);
    curated_usvs(k,2) = curated_usvs(k,2) - window_Begin(k);
end
function varargout = Plot(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Plot_OpeningFcn, ...
                   'gui_OutputFcn',  @Plot_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function Plot_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

movegui(gcf,'center')

handles.uipanel2.Visible = 'off';
handles.uipanel3.Visible = 'off';
handles.uipanel4.Visible = 'off';
handles.uipanel5.Visible = 'off';

setappdata(0, 'ini_t', 0);
setappdata(0, 'fini_t', 0);
setappdata(0, 'ini_f', 0);
setappdata(0, 'fini_f', 0);
setappdata(0, 'numb_usvs', 0);
audio = getappdata(0, 'audio_double');
%[yf,~] = eegfilt(audio',250000,20000,100000,0,[],0,'fir1',0);
[yf,~] = eegfilt(audio',250000,20000,100000);
yf = yf';
setappdata(0, 'audio_filt', yf);

setappdata(0, 'j', 0);

% M�ximo n�mero de sele��es no listbox
wb = getappdata(0, 'window_Begin');
nwb = getappdata(0, 'n_wb');
set(handles.listbox1,'Max',length(wb),'Min',0, 'Value', []);
set(handles.listbox3,'Max',length(nwb),'Min',0, 'Value', []);

setappdata(0, 'plot_s', 0);
setappdata(0, 'plot_e', 0);
setappdata(0, 'plot_t', 0);

Entropy = getappdata(0, 'Entropy');
audio_data = getappdata(0, 'audio_double');

setappdata(0, 'max_e', max(Entropy-getappdata(0, 'rms_entropy')));
setappdata(0, 'min_e', min(Entropy-getappdata(0, 'rms_entropy')));
setappdata(0, 'max_a', max(audio_data));
setappdata(0, 'min_a', min(audio_data));

handles.uipanel2.Visible = 'on';
setappdata(0, 'cm_plot', flipud(gray));
setappdata(0, 'titulo', []);



function varargout = Plot_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    CREATE FCN    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inicial time (ms)
function edit3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Final time (ms)
function edit5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Initial frequency (Hz)
function edit6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Final frequency (Hz)
function edit7_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Normal USVs
function listbox1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Extra USVs
function listbox3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Select number of USVs
function edit8_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Max. of USVs
function edit9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Color map
function popupmenu2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%     CALLBACKS     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Spectrogram
function checkbox1_Callback(hObject, eventdata, handles)
value = get(handles.checkbox1, 'Value');
if(value==1)
    setappdata(0, 'plot_s', 1);
else
    setappdata(0, 'plot_s', 0);
end
pushbutton3_Callback(hObject, eventdata, handles)

% Entropy
function checkbox2_Callback(hObject, eventdata, handles)
value = get(handles.checkbox2, 'Value');
if(value==1)
    setappdata(0, 'plot_e', 1);
else
    setappdata(0, 'plot_e', 0);
end
pushbutton3_Callback(hObject, eventdata, handles)

% Time
function checkbox3_Callback(hObject, eventdata, handles)
value = get(handles.checkbox3, 'Value');
if(value==1)
    setappdata(0, 'plot_t', 1);
else
    setappdata(0, 'plot_t', 0);
end
pushbutton3_Callback(hObject, eventdata, handles)

% Time range
function radiobutton1_Callback(hObject, eventdata, handles)
handles.uipanel2.Visible = 'on';
handles.uipanel3.Visible = 'off';
handles.uipanel4.Visible = 'off';
handles.uipanel5.Visible = 'off';
pushbutton3_Callback(hObject, eventdata, handles)

% Frequency range
function radiobutton2_Callback(hObject, eventdata, handles)
handles.uipanel2.Visible = 'off';
handles.uipanel3.Visible = 'on';
handles.uipanel4.Visible = 'off';
handles.uipanel5.Visible = 'off';

% Specific USVs
function radiobutton3_Callback(hObject, eventdata, handles)
handles.uipanel2.Visible = 'off';
handles.uipanel3.Visible = 'off';
handles.uipanel4.Visible = 'on';
handles.uipanel5.Visible = 'off';

curated_usvs = getappdata(0, 'curated_usvs');
NEW_curated_usvs = getappdata(0, 'n_cusv');

list_USV = [];
list_EUSV = [];
for i = 1:length(curated_usvs(:,1))
    if(curated_usvs(i,6)==1)
        list_USV = [list_USV curated_usvs(i,1)];
    end
end
for i = 1:length(NEW_curated_usvs(:,1))-1
    if(NEW_curated_usvs(i,6)==1)
        list_EUSV = [list_EUSV NEW_curated_usvs(i,1)];
    end
end
set(handles.listbox1, 'String', list_USV);
set(handles.listbox3, 'String', list_EUSV);

% Random USVs
function radiobutton4_Callback(hObject, eventdata, handles)
handles.uipanel2.Visible = 'off';
handles.uipanel3.Visible = 'off';
handles.uipanel4.Visible = 'off';
handles.uipanel5.Visible = 'on';

% Clear
function pushbutton3_Callback(hObject, eventdata, handles)
setappdata(0, 't1_plot', []);
setappdata(0, 't2_plot', []);
setappdata(0, 'S_temp', []);
setappdata(0, 'temp1', []);
setappdata(0, 'temp2', []);
setappdata(0, 'j', 0);

% Inicial time (s)
function edit3_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit3, 'String'));
if (isfinite(input)) 
    setappdata(0, 'ini_t', input);
end

% Final time (s)
function edit5_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit5, 'String'));
if (isfinite(input)) 
    setappdata(0, 'fini_t', input);
end

% Initial frequency (Hz)
function edit6_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit6, 'String'));
if (isfinite(input)) 
    setappdata(0, 'ini_f', input);
end

% Final frequency (Hz)
function edit7_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit7, 'String'));
if (isfinite(input)) 
    setappdata(0, 'fini_f', input);
end

% Select number of USVs
function edit8_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit8, 'String'));
if (isfinite(input)) 
    setappdata(0, 'numb_usvs', input);
end

% Max. of USVs
function edit9_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit9, 'String'));
if (isfinite(input)) 
    setappdata(0, 'max_usvs', input);
end



% Normal USVs
function listbox1_Callback(hObject, eventdata, handles)
ListUSV = get(handles.listbox1, 'value');
if(length(ListUSV)~=0)
    setappdata(0, 'list_USV', ListUSV);
end

% Extra USVs
function listbox3_Callback(hObject, eventdata, handles)
ListEUSV = get(handles.listbox3, 'value');
if(length(ListEUSV)~=0)
    setappdata(0, 'list_EUSV', ListEUSV);
end


% Add plot (time range)
function pushbutton4_Callback(hObject, eventdata, handles)
tt1 = getappdata(0, 'ini_t');
tt2 = getappdata(0, 'fini_t');
t = round(getappdata(0, 't'), 4);
Entropy = getappdata(0, 'Entropy');
rms_entropy = getappdata(0, 'rms_entropy');
thre = getappdata(0, 'thre');
audio_data = getappdata(0, 'audio_filt');
f = getappdata(0, 'f');
S = getappdata(0, 'S');
j = getappdata(0, 'j');
E_temp = getappdata(0, 'E_temp');
A_temp = getappdata(0, 'A_temp');
j = j+1;

t1 = getappdata(0, 't1_plot');
t2 = getappdata(0, 't2_plot');
S_temp1 = getappdata(0, 'S_temp');
temp1 = getappdata(0, 'temp1');
temp2 = getappdata(0, 'temp2');

temp1(j) = roundtowardvec(tt1, t, 'round');
temp2(j) = roundtowardvec(tt2, t, 'round');

t1(j) = find(t==temp1(j));
t2(j) = find(t==temp2(j));

setappdata(0, 't1_plot', t1);
setappdata(0, 't2_plot', t2);
setappdata(0, 'temp1', temp1);
setappdata(0, 'temp2', temp2);
setappdata(0, 'j', j);

cond_s = getappdata(0, 'plot_s');
cond_e = getappdata(0, 'plot_e');
cond_t = getappdata(0, 'plot_t');

figure;
if(getappdata(0, 'plot_s'))
    if(j==1)
        S_temp1 = {S(t1:t2,:)'};
        if t2 > length(S)
            t2 = length(S);
            S_temp1 = {S(t1:t2,:)'};    
        end
    else
        S_temp1 = [S_temp1 {S(t1(j):t2(j),:)'}];
        if t2(j) > length(S)
            t2(j) = length(S);
            S_temp1 = [S_temp1 {S(t1(j):t2(j),:)'}];    
        end
    end
    if(cond_e && cond_t)
        h1=subplot(4,4,1:8);
    end
    if((cond_e && ~cond_t) || (~cond_e && cond_t))
        h1=subplot(2,8,1:8);
    end
    
    plot_s(S_temp1, j, t, t1, t2, f, getappdata(0, 'cm_plot'));
    resetplotview(gca,'InitializeCurrentView');
    
    if(~cond_e && ~cond_t)
        xlabel('Time (s)')
    end
    setappdata(0, 'S_temp', S_temp1);
end
if(getappdata(0, 'plot_e'))
    % plot entropy
    if(j==1)
        E_temp = {Entropy(t1(j):t2(j))-rms_entropy};
    else
        E_temp = [E_temp {Entropy(t1(j):t2(j))-rms_entropy}];
    end
    if(cond_s && cond_t)
        h2=subplot(4,4,9:12);
    end
    if(cond_s && ~cond_t)
        h2=subplot(2,8,9:16);
    end
    if(~cond_s && cond_t)
        h2=subplot(2,8,1:8);
    end
   
    plot_e(E_temp, j, t, t1, t2, getappdata(0, 'thre'), getappdata(0, 'min_e'), getappdata(0, 'max_e') );
    resetplotview(gca,'InitializeCurrentView');

    if(~cond_t)
        xlabel('Time (s)')
    end
    if(cond_s)
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
    end
    setappdata(0, 'E_temp', E_temp);
end
if(getappdata(0, 'plot_t'))
    time1 = t(t1(j))*getappdata(0, 'fs');
    time2 = t(t2(j))*getappdata(0, 'fs');
    if(j==1)
        A_temp = {audio_data(time1:time2)};
    else
        A_temp = [A_temp {audio_data(time1:time2)}];
    end
    if(cond_s && cond_e)
        h3=subplot(4,4,13:16);
    end
    if(cond_s && ~cond_e)
        h3=subplot(2,8,9:16);
    end
    if(~cond_s && cond_e)
        h3=subplot(2,8,9:16);
    end
    
    plot_t(A_temp, j, t, t1, t2, audio_data, 1, getappdata(0, 'min_a'), getappdata(0, 'max_a') );
    resetplotview(gca,'InitializeCurrentView');
    
    if(cond_s)
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
    elseif(cond_e)
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
    end
    setappdata(0, 'A_temp', A_temp);
end
ax = [];
if(cond_s)
   try
       ax = [ax h1]; 
   catch
       ax = [ax gca];
   end
end
if(cond_e)
   try
       ax = [ax h2]; 
   catch
       ax = [ax gca];
   end 
end
if(cond_t)
   try
       ax = [ax h3]; 
   catch
       ax = [ax gca];
   end 
end
tit = getappdata(0, 'titulo');
name = getappdata(0, 'name');
intro = ', ploting in time range: ';
to = '/';
sss = 's';
sp = ' ';
dot = '; ';
one = num2str(tt1);
two = num2str(tt2);
if(j==1)
    tit = strcat(name, intro, one, sss, to, two, sss, dot);
    setappdata(0, 'titulo', tit);
else
    tit = strcat(tit, one, sss, to, two, sss, dot);
    setappdata(0, 'titulo', tit);
end

if(cond_s && (cond_e || cond_t))
    title(h1, tit)
elseif(~cond_s && cond_e && cond_t)
    title(h2, tit)
else
    title(gca, tit)
end
L1 = addlistener(gca, 'XLim',  'PostSet', @(~,~)cleanListen(ax, getappdata(0, 'intervals')));

% Add plot (frequency range)
function pushbutton5_Callback(hObject, eventdata, handles)
curated_usvs = getappdata(0, 'curated_usvs');
window_Begin = getappdata(0, 'window_Begin');
window_Dur = getappdata(0, 'window_Dur');
NEW_curated_usvs = getappdata(0, 'n_cusv');
NEW_window_Begin = getappdata(0, 'n_wb');
NEW_window_Dur = getappdata(0, 'n_wd');

t = round(getappdata(0, 't'), 4);
Entropy = getappdata(0, 'Entropy');
rms_entropy = getappdata(0, 'rms_entropy');
thre = getappdata(0, 'thre');
audio_data = getappdata(0, 'audio_filt');
f = getappdata(0, 'f');
S = getappdata(0, 'S');

% audio_data = decimate(audio_data, 500);
% while(length(audio_data)>length(t))
%     audio_data(end)=[];
% end

f_ini = getappdata(0, 'ini_f');
f_end = getappdata(0, 'fini_f');
max_usvs = getappdata(0, 'max_usvs');
k_USV = [];
k_EUSV = [];
% determina os �ndices das USVs na faixa de frequ�ncia especificada
for i = 1:length(NEW_window_Begin)
    higher = NEW_curated_usvs(i,3);
    lower = NEW_curated_usvs(i,3)+NEW_curated_usvs(i,5);
    if((higher>=f_ini) && (lower<=f_end))
        k_EUSV = [k_EUSV NEW_curated_usvs(i,1)];
    end
end
for i = 1:length(window_Begin)
    higher = curated_usvs(i,3);
    lower = curated_usvs(i,3)+curated_usvs(i,5);
    if((higher>=f_ini) && (lower<=f_end))
        k_USV = [k_USV curated_usvs(i,1)];
    end
end

% determina as USVs equivalente aos �ndices
for i = 1:length(k_USV)
%     temp1(i) = window_Begin(rand_k(i))+curated_usvs(rand_k(i),2);
%     temp2(i) = temp1(i)+curated_usvs(rand_k(i),4);
    temp1(i) = window_Begin(k_USV(i));
    temp2(i) = temp1(i)+window_Dur(k_USV(i));
    
    temp1(i) = roundtowardvec(temp1(i), t, 'round');
    temp2(i) = roundtowardvec(temp2(i), t, 'round');
    
    t1(i) = find(t==temp1(i));
    t2(i) = find(t==temp2(i));
end
for i = length(k_USV)+1:length(k_USV)+length(k_EUSV)
%     temp1(i) = window_Begin(rand_k(i))+curated_usvs(rand_k(i),2);
%     temp2(i) = temp1(i)+curated_usvs(rand_k(i),4);
    temp1(i) = NEW_window_Begin(k_EUSV(i-length(USV_k)));
    temp2(i) = temp1(i)+NEW_window_Dur(k_EUSV(i-length(USV_k)));
    
    temp1(i) = roundtowardvec(temp1(i), t, 'round');
    temp2(i) = roundtowardvec(temp2(i), t, 'round');
    
    t1(i) = find(t==temp1(i));
    t2(i) = find(t==temp2(i));
end

t1 = sort(t1);
t2 = sort(t2);
temp1 = sort(temp1);
temp2 = sort(temp2);

m = length(t1);
if(max_usvs<m)
    rand_k = randperm(m, max_usvs);
    rand_k = sort(rand_k);
    for i = 1:length(rand_k)
        tt1(i) = t1(rand_k(i));
        tt2(i) = t2(rand_k(i));
        ttemp1(i) = temp1(rand_k(i));
        ttemp2(i) = temp2(rand_k(i));
    end
    t1 = tt1;
    t2 = tt2;
    temp1 = ttemp1;
    temp2 = ttemp2;
end



n = length(t1);

cond_s = getappdata(0, 'plot_s');
cond_e = getappdata(0, 'plot_e');
cond_t = getappdata(0, 'plot_t');

figure;
if(getappdata(0, 'plot_s')==1)
    % definindo peda�os do espectrograma
    for i = 1:n
        if(i==1)
            S_temp1 = {S(t1(i):t2(i),:)'};
            if t2(i) > length(S)
                t2(i) = length(S);
                S_temp1 = {S(t1(i):t2(i),:)'};    
            end
        else
            S_temp1 = [S_temp1 {S(t1(i):t2(i),:)'}];
            if t2(i) > length(S)
                t2(i) = length(S);
                S_temp1 = [S_temp1 {S(t1(i):t2(i),:)'}];    
            end
        end
    end
    if(cond_e && cond_t)
        h1=subplot(4,4,1:8);
    end
    if((cond_e && ~cond_t) || (~cond_e && cond_t))
        h1=subplot(2,8,1:8);
    end
    
    plot_s(S_temp1, n, t, t1, t2, f, getappdata(0, 'cm_plot'));   
    if(~cond_e && ~cond_t)
        xlabel('Time (s)')
    end
end
if(getappdata(0, 'plot_e')==1)
    % plot entropy
    for i = 1:n
        if(i==1)
            E_temp = {Entropy(t1(i):t2(i))-rms_entropy};
        else
            E_temp = [E_temp {Entropy(t1(i):t2(i))-rms_entropy}];
        end
    end
    if(cond_s && cond_t)
        h2=subplot(4,4,9:12);
    end
    if(cond_s && ~cond_t)
        h2=subplot(2,8,9:16);
    end
    if(~cond_s && cond_t)
        h2=subplot(2,8,1:8);
    end
   
    plot_e(E_temp, n, t, t1, t2, getappdata(0, 'thre'), getappdata(0, 'min_e'), getappdata(0, 'max_e') );
    if(~cond_t)
        xlabel('Time (s)')
    end
    if(cond_s)
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
    end
end
if(getappdata(0, 'plot_t')==1)
    for i = 1:n 
        time1 = t(t1(i))*getappdata(0, 'fs');
        time2 = t(t2(i))*getappdata(0, 'fs');
        if(i==1)
            A_temp = {audio_data(time1:time2)};
        else
            A_temp = [A_temp {audio_data(time1:time2)}];
        end
    end
    
    if(cond_s && cond_e)
        h3=subplot(4,4,13:16);
    end
    if(cond_s && ~cond_e)
        h3=subplot(2,8,9:16);
    end
    if(~cond_s && cond_e)
        h3=subplot(2,8,9:16);
    end
    
    plot_t(A_temp, n, t, t1, t2, audio_data, 0, getappdata(0, 'min_a'), getappdata(0, 'max_a') );
    
    if(cond_s)
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
    elseif(cond_e)
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
    end
end


name = getappdata(0, 'name');
intro = ', ploting USVs number: ';
dot = '; ';
tit = strcat(name, intro, num2str(rand_k), dot);

if(cond_s && (cond_e || cond_t))
    title(h1, tit)
elseif(~cond_s && cond_e && cond_t)
    title(h2, tit)
else
    title(gca, tit)
end

% Add plot (random USVs)
function pushbutton6_Callback(hObject, eventdata, handles)
size = getappdata(0, 'numb_usvs');
curated_usvs = getappdata(0, 'curated_usvs');
window_Begin = getappdata(0, 'window_Begin');
window_Dur = getappdata(0, 'window_Dur');
NEW_curated_usvs = getappdata(0, 'n_cusv');
NEW_window_Begin = getappdata(0, 'n_wb');
NEW_window_Dur = getappdata(0, 'n_wd');

t = round(getappdata(0, 't'), 4);
Entropy = getappdata(0, 'Entropy');
rms_entropy = getappdata(0, 'rms_entropy');
thre = getappdata(0, 'thre');
audio_data = getappdata(0, 'audio_filt');
f = getappdata(0, 'f');
S = getappdata(0, 'S');



NEW_curated_usvs(end,:) = [];
B = [curated_usvs, window_Dur, window_Begin; NEW_curated_usvs, NEW_window_Dur, NEW_window_Begin];
[~,inx]=sort(B(:,end));
out = B(inx,:);
temp = [];
for i = 1:length(out(:, 1))
    if(~(out(i, 6)==0))
        temp = [temp; out(i, :)];
    end
end
n = length(temp(:,1));
curated_usvs = temp(1:n, 1:6);
window_Begin = temp(1:n, 8);
window_Dur = temp(1:n, 7);
for i = 1:length(window_Begin)
    if(window_Begin(i)<0)
        window_Begin(i) = 0.005;
    end
end
m = length(window_Begin);
rand_k = randperm(m, size);
rand_k = sort(rand_k);
n = length(rand_k);

for i = 1:n
%     temp1(i) = window_Begin(rand_k(i))+curated_usvs(rand_k(i),2);
%     temp2(i) = temp1(i)+curated_usvs(rand_k(i),4);
    temp1(i) = window_Begin(rand_k(i));
    temp2(i) = temp1(i)+window_Dur(rand_k(i));
    
    temp1(i) = roundtowardvec(temp1(i), t, 'round');
    temp2(i) = roundtowardvec(temp2(i), t, 'round');
    
    t1(i) = find(t==temp1(i));
    t2(i) = find(t==temp2(i));
end

cond_s = getappdata(0, 'plot_s');
cond_e = getappdata(0, 'plot_e');
cond_t = getappdata(0, 'plot_t');

figure;
if(getappdata(0, 'plot_s'))
    % definindo peda�os do espectrograma
    for i = 1:n
        if(i==1)
            S_temp1 = {S(t1(i):t2(i),:)'};
            if t2(i) > length(S)
                t2(i) = length(S);
                S_temp1 = {S(t1(i):t2(i),:)'};    
            end
        else
            S_temp1 = [S_temp1 {S(t1(i):t2(i),:)'}];
            if t2(i) > length(S)
                t2(i) = length(S);
                S_temp1 = [S_temp1 {S(t1(i):t2(i),:)'}];    
            end
        end
    end
    if(cond_e && cond_t)
        h1=subplot(4,4,1:8);
    end
    if((cond_e && ~cond_t) || (~cond_e && cond_t))
        h1=subplot(2,8,1:8);
    end
    
    plot_s(S_temp1, n, t, t1, t2, f, getappdata(0, 'cm_plot'));   
    if(~cond_e && ~cond_t)
        xlabel('Time (s)')
    end
end
if(getappdata(0, 'plot_e'))
    % plot entropy
    for i = 1:n
        if(i==1)
            E_temp = {Entropy(t1(i):t2(i))-rms_entropy};
        else
            E_temp = [E_temp {Entropy(t1(i):t2(i))-rms_entropy}];
        end
    end
    if(cond_s && cond_t)
        h2=subplot(4,4,9:12);
    end
    if(cond_s && ~cond_t)
        h2=subplot(2,8,9:16);
    end
    if(~cond_s && cond_t)
        h2=subplot(2,8,1:8);
    end
   
    plot_e(E_temp, n, t, t1, t2, getappdata(0, 'thre'), getappdata(0, 'min_e'), getappdata(0, 'max_e') );
    
    if(~cond_t)
        xlabel('Time (s)')
    end
    if(cond_s)
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
    end
end
if(getappdata(0, 'plot_t'))
    for i = 1:n 
        time1 = t(t1(i))*getappdata(0, 'fs');
        time2 = t(t2(i))*getappdata(0, 'fs');
        if(i==1)
            A_temp = {audio_data(time1:time2)};
        else
            A_temp = [A_temp {audio_data(time1:time2)}];
        end
    end
    if(cond_s && cond_e)
        h3=subplot(4,4,13:16);
    end
    if(cond_s && ~cond_e)
        h3=subplot(2,8,9:16);
    end
    if(~cond_s && cond_e)
        h3=subplot(2,8,9:16);
    end
    
    plot_t(A_temp, n, t, t1, t2, audio_data, 0, getappdata(0, 'min_a'), getappdata(0, 'max_a') );
    
    if(cond_s)
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
    elseif(cond_e)
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
    end
end

name = getappdata(0, 'name');
intro = ', ploting USVs number: ';
dot = '; ';
tit = strcat(name, intro, num2str(rand_k), dot);

if(cond_s && (cond_e || cond_t))
    title(h1, tit)
elseif(~cond_s && cond_e && cond_t)
    title(h2, tit)
else
    title(gca, tit)
end

% Add specific USVs
function pushbutton7_Callback(hObject, eventdata, handles)
curated_usvs = getappdata(0, 'curated_usvs');
window_Begin = getappdata(0, 'window_Begin');
window_Dur = getappdata(0, 'window_Dur');
NEW_curated_usvs = getappdata(0, 'n_cusv');
NEW_window_Begin = getappdata(0, 'n_wb');
NEW_window_Dur = getappdata(0, 'n_wd');

t = round(getappdata(0, 't'), 4);
Entropy = getappdata(0, 'Entropy');
rms_entropy = getappdata(0, 'rms_entropy');
thre = getappdata(0, 'thre');
audio_data = getappdata(0, 'audio_filt');
f = getappdata(0, 'f');
S = getappdata(0, 'S');
USV_k = getappdata(0, 'list_USV');
EUSV_k = getappdata(0, 'list_EUSV');



for i = 1:length(USV_k)
%     temp1(i) = window_Begin(rand_k(i))+curated_usvs(rand_k(i),2);
%     temp2(i) = temp1(i)+curated_usvs(rand_k(i),4);
    temp1(i) = window_Begin(USV_k(i));
    temp2(i) = temp1(i)+window_Dur(USV_k(i));
    
    temp1(i) = roundtowardvec(temp1(i), t, 'round');
    temp2(i) = roundtowardvec(temp2(i), t, 'round');
    
    t1(i) = find(t==temp1(i));
    t2(i) = find(t==temp2(i));
end
for i = length(USV_k)+1:length(USV_k)+length(EUSV_k)
%     temp1(i) = window_Begin(rand_k(i))+curated_usvs(rand_k(i),2);
%     temp2(i) = temp1(i)+curated_usvs(rand_k(i),4);
    temp1(i) = NEW_window_Begin(EUSV_k(i-length(USV_k)));
    temp2(i) = temp1(i)+NEW_window_Dur(EUSV_k(i-length(USV_k)));
    
    temp1(i) = roundtowardvec(temp1(i), t, 'round');
    temp2(i) = roundtowardvec(temp2(i), t, 'round');
    
    t1(i) = find(t==temp1(i));
    t2(i) = find(t==temp2(i));
end

t1 = sort(t1);
t2 = sort(t2);

n = length(t1);

cond_s = getappdata(0, 'plot_s');
cond_e = getappdata(0, 'plot_e');
cond_t = getappdata(0, 'plot_t');

figure;
if(getappdata(0, 'plot_s')==1)
    % definindo peda�os do espectrograma
    for i = 1:n
        if(i==1)
            S_temp1 = {S(t1(i):t2(i),:)'};
            if t2(i) > length(S)
                t2(i) = length(S);
                S_temp1 = {S(t1(i):t2(i),:)'};    
            end
        else
            S_temp1 = [S_temp1 {S(t1(i):t2(i),:)'}];
            if t2(i) > length(S)
                t2(i) = length(S);
                S_temp1 = [S_temp1 {S(t1(i):t2(i),:)'}];    
            end
        end
    end
    if(cond_e && cond_t)
        h1=subplot(4,4,1:8);
    end
    if((cond_e && ~cond_t) || (~cond_e && cond_t))
        h1=subplot(2,8,1:8);
    end
    
    plot_s(S_temp1, n, t, t1, t2, f, getappdata(0, 'cm_plot'));   
    if(~cond_e && ~cond_t)
        xlabel('Time (s)')
    end
end
if(getappdata(0, 'plot_e')==1)
    % plot entropy
    for i = 1:n
        if(i==1)
            E_temp = {Entropy(t1(i):t2(i))-rms_entropy};
        else
            E_temp = [E_temp {Entropy(t1(i):t2(i))-rms_entropy}];
        end
    end
    if(cond_s && cond_t)
        h2=subplot(4,4,9:12);
    end
    if(cond_s && ~cond_t)
        h2=subplot(2,8,9:16);
    end
    if(~cond_s && cond_t)
        h2=subplot(2,8,1:8);
    end
   
    plot_e(E_temp, n, t, t1, t2, getappdata(0, 'thre'), getappdata(0, 'min_e'), getappdata(0, 'max_e') );
    if(~cond_t)
        xlabel('Time (s)')
    end
    if(cond_s)
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
        linkaxes([h1,h2],'x')
    end
end
if(getappdata(0, 'plot_t')==1)
    for i = 1:n 
        time1 = t(t1(i))*getappdata(0, 'fs');
        time2 = t(t2(i))*getappdata(0, 'fs');
        if(i==1)
            A_temp = {audio_data(time1:time2)};
        else
            A_temp = [A_temp {audio_data(time1:time2)}];
        end
    end
    
    if(cond_s && cond_e)
        h3=subplot(4,4,13:16);
    end
    if(cond_s && ~cond_e)
        h3=subplot(2,8,9:16);
    end
    if(~cond_s && cond_e)
        h3=subplot(2,8,9:16);
    end
   
    
    plot_t(A_temp, n, t, t1, t2, audio_data, 0, getappdata(0, 'min_a'), getappdata(0, 'max_a') );
    
    if(cond_s)
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
        linkaxes([h1,h3],'x')
    elseif(cond_e)
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
        linkaxes([h2,h3],'x')
    end
end
% setappdata(0, 'list_USV', []);
% setappdata(0, 'list_EUSV', []);

name = getappdata(0, 'name');
intro = ', ploting USVs number: ';
intro1 = ', Normal USVs(';
clos = ')';
intro2 = ', Extra USVs(';
normal = num2str(USV_k(1:end));
extras = num2str(EUSV_k(1:end));
dot = '; ';
tit = strcat(name, intro, intro1, normal, clos, dot, intro2, extras, clos, dot);

if(cond_s && (cond_e || cond_t))
    title(h1, tit)
elseif(~cond_s && cond_e && cond_t)
    title(h2, tit)
else
    title(gca, tit)
end


% Color map
function popupmenu2_Callback(hObject, eventdata, handles)
contents = get(handles.popupmenu2,'String'); 
pop_choice = contents{get(handles.popupmenu2,'Value')};
switch pop_choice
    case 'parula'
        temp = parula;
    case 'hot'
        temp = hot;
    case 'gray'
        temp = flipud(gray);
    otherwise
        temp = copper;
end
setappdata(0, 'cm_plot', temp);



function edit10_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit10, 'String'));
if (isfinite(input))    
    input = input*1e-8;
    setappdata(0, 'intensity', input);
end

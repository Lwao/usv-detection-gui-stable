function varargout = Analysis(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Analysis_OpeningFcn, ...
                   'gui_OutputFcn',  @Analysis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function Analysis_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

Ccu = getappdata(0, 'curated_usvs');
Cwb = getappdata(0, 'window_Begin');
Ocu = getappdata(0, 'ORI_curated_usvs');
Owb = getappdata(0, 'ORI_window_Begin');
Ncu = getappdata(0, 'n_cusv');
Nwb = getappdata(0, 'n_wb');

n = length(Cwb);

tiO = [];
tiC = [];
tfO = [];
tfC = [];
fiO = [];
fiC = [];
ffO = [];
ffC = [];

for i = 1:n
    if(Ccu(i,6)==1)
        tiO = [tiO, Ocu(i,2)+Owb(i)];
        tiC = [tiC, Ccu(i,2)+Cwb(i)];

        tfO = [tfO, Ocu(i,2)+Owb(i)+Ocu(i,4)];
        tfC = [tfC, Ccu(i,2)+Cwb(i)+Ccu(i,4)];

        fiO = [fiO, Ocu(i,3)];
        fiC = [fiC, Ccu(i,3)];

        ffO = [ffO, Ocu(i,3)+Ocu(i,5)];
        ffC = [ffC, Ccu(i,3)+Ccu(i,5)];
    end
    
end
rms_ti = sqrt((1/n)*sum((tiO-tiC).^2))*1000;
rms_tf = sqrt((1/n)*sum((tfO-tfC).^2))*1000;
rms_fi = sqrt((1/n)*sum((fiO-fiC).^2));
rms_ff = sqrt((1/n)*sum((ffO-ffC).^2));

myString = sprintf('Initial time RMS: %f (ms)\nFinal time RMS: %f (ms)\nInitial frequency RMS: %f (kHz)\nFinal frequency RMS: %f (kHz)\n\n\n\n\n\n\n', rms_ti, rms_tf, rms_fi, rms_ff);
set(handles.text6, 'String', myString);

% Predicted NO
TN = 0;
FN = 0;
for i = 1:length(Nwb)
   if(Ncu(i,6)==1) FN=FN+1; else TN=TN+1; end
end
% Predicted YES
TP = 0;
FP = 0;
%for i = 1:length(Cwb)
for i = 1:getappdata(0, 'k')
   if(Ccu(i,6)==1) TP=TP+1; else FP=FP+1; end
end

Accuracy = (TN+TP)/(TN+FP+FN+TP);
Precision = TP/(FP+TP);
Sensitivity = TP/(TP+FN);
Specificity = TN/(TN+FP);

myString = sprintf('Accuracy: %f\nPrecision: %f\nSensitivity: %f\nSpecificity: %f\n\n\n\n\n\n\n', Accuracy, Precision, Sensitivity, Specificity);
set(handles.text9, 'String', myString);

myString = sprintf('True positives: %d\nTrue negatives: %d\nFalse positives: %d\nFalse negatives: %d\n\n\n\n\n\n\n', TP, TN, FP, FN);
set(handles.text10, 'String', myString);





% set(handles.text6, 'String', strcat('Initial time RMS: ', num2str(rms_ti)));
% set(handles.text6, 'String', strcat('Final time RMS: ', num2str(rms_tf)));
% set(handles.text6, 'String', strcat('Initial frequency RMS: ', num2str(rms_fi)));
% set(handles.text6, 'String', strcat('Final frequency RMS: ', num2str(rms_ff)));


function varargout = Analysis_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;



function pushbutton_sheet_Callback(hObject, eventdata, handles)

C_curated_usvs = getappdata(0, 'curated_usvs');
C_window_begin = getappdata(0, 'window_Begin');
O_curated_usvs = getappdata(0, 'ORI_curated_usvs');
O_window_begin = getappdata(0, 'ORI_window_Begin');
N_curated_usvs = getappdata(0, 'n_cusv');
N_window_begin = getappdata(0, 'n_wb');


c = length(C_window_begin);
n = length(N_window_begin);

TP = 0;
FP = 0;
for itr = 1:c
    % corrected values
    initial_time_corrected(itr) = C_curated_usvs(itr,2)+C_window_begin(itr);
    time_duration_corrected(itr) = C_curated_usvs(itr,4);
    final_time_corrected(itr) = initial_time_corrected(itr)+time_duration_corrected(itr);
    mean_time_corrected(itr) = initial_time_corrected(itr) + time_duration_corrected(itr)/2;
    
    minimum_frequency_corrected(itr) = C_curated_usvs(itr,3);
    maximum_frequency_corrected(itr) = C_curated_usvs(itr,5);
    frequency_variation_corrected(itr) = maximum_frequency_corrected(itr)-minimum_frequency_corrected(itr);
    mean_frequency_corrected(itr) = minimum_frequency_corrected(itr) + frequency_variation_corrected(itr)/2;
    
    % original values
    initial_time_original(itr) = O_curated_usvs(itr,2)+O_window_begin(itr);
    time_duration_original(itr) = O_curated_usvs(itr,4);
    final_time_original(itr) = initial_time_original(itr)+time_duration_original(itr);
    mean_time_original(itr) = initial_time_original(itr) + time_duration_original(itr)/2;
    
    minimum_frequency_original(itr) = O_curated_usvs(itr,3);
    maximum_frequency_original(itr) = O_curated_usvs(itr,5);
    frequency_variation_original(itr) = maximum_frequency_original(itr)-minimum_frequency_original(itr);
    mean_frequency_original(itr) = minimum_frequency_original(itr) + frequency_variation_original(itr)/2;
    
    accepted(itr) = C_curated_usvs(itr,6)==1;
    if(accepted(itr)==1) 
        classification{itr,1} = 'TP';
        TP = TP+1;
    else
        classification{itr,1} = 'FP'; 
        FP = FP+1;
    end
end

FN = 0;
TN = 0;
for itr = c+1:c+n
    % corrected values
    initial_time_corrected(itr) = N_curated_usvs(itr-c,2)+N_window_begin(itr-c);
    time_duration_corrected(itr) = N_curated_usvs(itr-c,4);
    final_time_corrected(itr) = initial_time_corrected(itr)+time_duration_corrected(itr);
    mean_time_corrected(itr) = initial_time_corrected(itr) + time_duration_corrected(itr)/2;
    
    minimum_frequency_corrected(itr) = N_curated_usvs(itr-c,3);
    maximum_frequency_corrected(itr) = N_curated_usvs(itr-c,5);
    frequency_variation_corrected(itr) = maximum_frequency_corrected(itr)-minimum_frequency_corrected(itr);
    mean_frequency_corrected(itr) = minimum_frequency_corrected(itr) + frequency_variation_corrected(itr)/2;
    
    % original values
    initial_time_original(itr) = NaN;
    time_duration_original(itr) = NaN;
    final_time_original(itr) = NaN;
    mean_time_original(itr) = NaN;
    
    minimum_frequency_original(itr) = NaN;
    maximum_frequency_original(itr) = NaN;
    frequency_variation_original(itr) = NaN;
    mean_frequency_original(itr) = NaN;
    
    accepted(itr) = N_curated_usvs(itr-c,6)==1;
    if(accepted(itr)==1) 
        classification{itr,1} = 'FN';
        FN = FN+1;
    else
        classification{itr,1} = 'TN'; 
        TN = TN+1;
    end
end

Accuracy = (TN+TP)/(TN+FP+FN+TP);
Precision = TP/(FP+TP);
Sensitivity = TP/(TP+FN);
Specificity = TN/(TN+FP);

T = table(initial_time_corrected(:),time_duration_corrected(:),final_time_corrected(:),mean_time_corrected(:),...
    minimum_frequency_corrected(:),maximum_frequency_corrected(:),frequency_variation_corrected(:),mean_frequency_corrected(:),...
    initial_time_original(:),time_duration_original(:),final_time_original(:),mean_time_original(:),...
    minimum_frequency_original(:),maximum_frequency_original(:),frequency_variation_original(:),mean_frequency_original(:),...
    accepted(:),classification,...
    'VariableNames',...
    {'initial_time_corrected','time_duration_corrected','final_time_corrected','mean_time_corrected',...
    'minimum_frequency_corrected','maximum_frequency_corrected','frequency_variation_corrected','mean_frequency_corrected',...
    'initial_time_original','time_duration_original','final_time_original','mean_time_original',...
    'minimum_frequency_original','maximum_frequency_original','frequency_variation_original','mean_frequency_original',...
    'accepted','classification'});

[file,path,indx] = uiputfile(['Sheet_' '.xls']);
writetable(T,[path file])

function varargout = Curating(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Curating_OpeningFcn, ...
                   'gui_OutputFcn',  @Curating_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function Curating_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

movegui(gcf,'center')

set(handles.pushbutton1,'Enable','off');
set(handles.pushbutton2,'Enable','off');
set(handles.pushbutton3,'Enable','off');
set(handles.pushbutton4,'Enable','off');
set(handles.pushbutton5,'Enable','off');
set(handles.pushbutton12,'Enable','off');
set(handles.pushbutton15,'Enable','off');

% Axes1
axes(handles.axes1)
intensity = 4e-8;
title(getappdata(0, 'name'))
hold on
box off
axis xy
ylabel('Frequency(kHz)')
caxis([0 intensity])
colormap(flipud(gray))
hold off
freq_range = getappdata(0,'freq_range');
ylim(freq_range)
% Axes2
axes(handles.axes2)
box off
hold on
ylabel('Norm. Entropy')
% Axes3
axes(handles.axes3)
hold on
box off
axis xy
xlabel('Time (s)')
ylabel('Frequency(kHz)')
caxis([0 intensity])
colormap(flipud(gray))
hold off
ylim(freq_range)

% Inicializa spacer e intensity
spacer = 0.1;
intensity = 4e-8;
setappdata(0, 'spacer', spacer);
setappdata(0, 'intensity', intensity);
wait_bar = waitbar(0,'Loading...');
if(getappdata(0, 'transition3')==0)
    % Inicializa curated_usvs e iterador
    USV_data = getappdata(0, 'USV_data');
    S = getappdata(0, 'S');
    t = getappdata(0, 't');
    step = getappdata(0, 'STEP');
    idx_ini = getappdata(0, 'idx_ini');
    idx_end = getappdata(0, 'idx_end');
    curated_usvs(:, 2:5) = USV_data;
    for k = 1:length(idx_ini)
        waitbar(k/length(idx_ini), wait_bar);
        len_S = length(S);
        t1 = idx_ini - spacer/step;
        t2 = idx_end + spacer/step;

        t2(t2 > len_S) = len_S;
        t1(t1 < 1) = 1;

        S_temp1 = S(t1(k):t2(k),:)';
        if k < length(idx_ini)-2
            S_temp2 = S(t1(k):t2(k+2),:)';
        else
            S_temp2 = S(t1(k):t2(end),:)';
        end

        if t2(k) > len_S
            t2(k) = len_S;
            S_temp1 = S(t1(k):t2(k),:)';    
        end 

        % Confirmando todas com as janelas padr�es
        window_Begin(k,1) = t(t1(k));
        window_Dur(k,1) = t(t2(k))-t(t1(k));
        curated_usvs(k,1) = k;
        curated_usvs(k,2) = curated_usvs(k,2) - window_Begin(k);
        curated_usvs(k,6) = 1;
    end
    iterator = 1;
    setappdata(0, 'k', iterator);
    setappdata(0, 'curated_usvs', curated_usvs);
    setappdata(0, 'window_Begin', window_Begin);
    setappdata(0, 'window_Dur', window_Dur);
    setappdata(0, 'ORI_curated_usvs', curated_usvs);
    setappdata(0, 'ORI_window_Begin', window_Begin);
    setappdata(0, 'ORI_window_Dur', window_Dur);
    set(handles.pushbutton8,'Enable','off')

    % Total de USVs
    set(handles.text7, 'String', strcat('Total USVs: ', num2str(length(idx_ini))));

    % Inicializando slider
    set(handles.slider2, 'Min', 1);
    set(handles.slider2, 'Max', length(idx_ini));
    set(handles.slider2, 'SliderStep', [1/length(idx_ini), 5/length(idx_ini)]);
    set(handles.slider2, 'Value', 1);

    % Inicializa as posi��es dos imrects
    matrix_h = zeros(length(idx_ini), 4);
    color_h = zeros(length(idx_ini), 3);
    color_h(1:length(color_h(:, 1)), 2) = 1; % todos cor verde claro (confirmado)
    setappdata(0, 'm_h', matrix_h);
    setappdata(0, 'c_h', color_h);

    % USVs extras
    setappdata(0, 'n_cusv', [0 0 0 0 0 0]);
    setappdata(0, 'n_wb', []);
    setappdata(0, 'n_wd', []);
    setappdata(0, 'n_k', []);
    
    set(handles.pushbutton17,'Enable','off')
end

idxx = length(getappdata(0, 'idx_ini'));
if (idxx==1)
    set(handles.pushbutton1,'Enable','off');
    set(handles.pushbutton8,'Enable','off');
end

    
% Vari�vel de escolha (0-confirma USV, 1-confirma NEW_USV)
setappdata(0, 'choice', 0);

% Dois refreshes para ajustar a janela
pushbutton2_Callback(hObject, eventdata, handles);
pushbutton2_Callback(hObject, eventdata, handles);

set(handles.pushbutton1,'Enable','on');
set(handles.pushbutton2,'Enable','on');
set(handles.pushbutton3,'Enable','on');
set(handles.pushbutton4,'Enable','on');
set(handles.pushbutton5,'Enable','on');
set(handles.pushbutton12,'Enable','on');
set(handles.pushbutton15,'Enable','on');

close(wait_bar);

function varargout = Curating_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    CREATE FCN    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function axes1_CreateFcn(hObject, eventdata, handles)
function axes2_CreateFcn(hObject, eventdata, handles)
function axes3_CreateFcn(hObject, eventdata, handles)

% Spacer
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Intensity
function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Escolher o colormap
function popupmenu3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Slider
function slider2_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%     CALLBACKS     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Next
function pushbutton1_Callback(hObject, eventdata, handles)
iterator = getappdata(0, 'k');
idx = length(getappdata(0, 'idx_ini'));
% Desabilita o NEXT, caso esteja na �ltima USV
if (iterator < idx-1)
    iterator = iterator + 1;
else
    iterator = iterator + 1;
    set(handles.pushbutton1,'Enable','off');
end
% Habilita o PREVIOUS se estiver ap�s a primeira USV
if (iterator > 1)
    set(handles.pushbutton8,'Enable','on');
else
    set(handles.pushbutton8,'Enable','off');
end
setappdata(0, 'k', iterator);
set(handles.slider2, 'Value', iterator);
pushbutton2_Callback(hObject, eventdata, handles);

% Previous
function pushbutton8_Callback(hObject, eventdata, handles)
iterator = getappdata(0, 'k');
idx = length(getappdata(0, 'idx_ini'));
% Desabilita o PREVIOUS, caso esteja na primeira USV
if (iterator > 2)
    iterator = iterator - 1;
else
    iterator = iterator - 1;
    set(handles.pushbutton8,'Enable','off');
end
% Habilita o NEXT se estiver antes da �ltima USV
if (iterator < idx)
    set(handles.pushbutton1,'Enable','on')
else
    set(handles.pushbutton1,'Enable','off')
end
setappdata(0, 'k', iterator);
set(handles.slider2, 'Value', iterator);
pushbutton2_Callback(hObject, eventdata, handles);

% Spacer
function edit1_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit1, 'String'));
if (isfinite(input)) 
    setappdata(0, 'spacer', input);
    pushbutton2_Callback(hObject, eventdata, handles);
end

% Intensity
function edit2_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit2, 'String'));
if (isfinite(input))    
    input = input*1e-8;
    setappdata(0, 'intensity', input);
    pushbutton2_Callback(hObject, eventdata, handles);
end

% Refresh
function pushbutton2_Callback(hObject, eventdata, handles)
cla(handles.axes1);
cla(handles.axes2);
cla(handles.axes3);

% Inicia curagem
[mat_rect, rect, temp1, temp2] = curating(...
    getappdata(0, 'n_k'), getappdata(0, 'n_cusv'), getappdata(0, 'n_wb'),...
    getappdata(0, 'c_h'), getappdata(0, 'm_h'), getappdata(0, 'name'), getappdata(0, 'color_map'),...
    getappdata(0, 'k'), getappdata(0, 'spacer'), getappdata(0, 'intensity'), getappdata(0, 'idx_ini'),...
    getappdata(0, 'idx_end'), getappdata(0, 'STEP'), getappdata(0, 'S'), getappdata(0, 't'),...
    getappdata(0, 'thre'), getappdata(0, 'Entropy'), getappdata(0, 'rms_entropy'), getappdata(0, 'f'),...
    getappdata(0, 'USV_data'), getappdata(0,'freq_range'), handles.axes1, handles.axes2, handles.axes3);
number = num2str(getappdata(0, 'k'), 5);
set(handles.text4, 'String', strcat('Putative USV no. ', number));

% Lan�a as vari�veis
setappdata(0, 'h', rect);
setappdata(0, 't1', temp1);
setappdata(0, 't2', temp2);
setappdata(0, 'm_h', mat_rect);

% Trata o status
itr = getappdata(0, 'k');
curated = getappdata(0, 'curated_usvs');
if (curated(itr, 6)==0) %primeira diferente de 0 e segunda igual a 0
    set(handles.text5, 'String', strcat('Status: Rejected'));
elseif (curated(itr, 6)~=0) %primeira e segunda coluna diferente de 0
    set(handles.text5, 'String', strcat('Status: Confirmed'));
end

% Confirm
function pushbutton3_Callback(hObject, eventdata, handles)
if (getappdata(0, 'choice')==0)
    % Confirma a sele��o
    %try
    [curated_temp, wb, wd] = confirm(...
        getappdata(0, 'window_Begin'), getappdata(0, 'window_Dur'),...
        getappdata(0, 'curated_usvs'), getappdata(0, 'k'), getappdata(0, 't'),...
        getappdata(0, 't1'), getappdata(0, 't2'), getappdata(0, 'h'));
    
    % Lan�a vari�veis
    setappdata(0, 'curated_usvs', curated_temp);
    setappdata(0, 'window_Begin', wb);
    setappdata(0, 'window_Dur', wd);
    
    itr = getappdata(0, 'k');
    
    % Status para nova confirma��o
    color = getappdata(0, 'c_h');
    color(itr, :) = [0 0.6 0];
    setappdata(0, 'c_h', color);
    set(handles.text5, 'String', strcat('Status: Confirmed'));
    
    % Mantem a janela
    matrix_h = getappdata(0, 'm_h');
    matrix_h(itr, :) = getPosition(getappdata(0, 'h'));
    setappdata(0, 'm_h', matrix_h);
    
    pushbutton2_Callback(hObject, eventdata, handles);
    delete(getappdata(0, 'h'));
    % Muda de USV
    if (itr~=length(getappdata(0, 'idx_ini')))
        pushbutton1_Callback(hObject, eventdata, handles);
    end
    %end
elseif(getappdata(0, 'choice')==1)
    %[0 0.6 0.8] cyan
    [NEW_k, NEW_curated_usvs, NEW_window_Begin, NEW_window_Dur] = new_detection(...
        getappdata(0, 'spacer'), getappdata(0, 'STEP'), getappdata(0, 'n_k'),...
        getappdata(0, 'n_h'), getappdata(0, 'n_wb'), getappdata(0, 'n_wd'),...
        getappdata(0, 'n_cusv'), getappdata(0, 'k'), getappdata(0, 't'),...
        getappdata(0, 't1'), getappdata(0, 't2'));
    
    setappdata(0, 'n_cusv', NEW_curated_usvs);
    setappdata(0, 'n_wb', NEW_window_Begin);
    setappdata(0, 'n_wd', NEW_window_Dur);
    setappdata(0, 'n_k', NEW_k);
    setappdata(0, 'choice', 0);
    pushbutton2_Callback(hObject, eventdata, handles);
    set(handles.pushbutton17,'Enable','on');
    delete(h_NEW);
end

% Redefine
function pushbutton4_Callback(hObject, eventdata, handles)
% Redefine o ret�ngulo
[curated_temp, wb, wd, h_temp] = redefine(...
    getappdata(0, 'h'), getappdata(0, 'curated_usvs'), getappdata(0, 'k'),...
    getappdata(0, 't1'), getappdata(0, 't2'),...
    getappdata(0, 't'), handles.axes1);
itr = getappdata(0, 'k');
%try
    matrix_h(itr, :) = getPosition(h_temp);
%catch
%    matrix_h(itr, :)= [0 0 0 0];
%end
% Lan�a vari�veis
% setappdata(0, 'curated_usvs', curated_temp);
setappdata(0, 'h', h_temp);
% setappdata(0, 'window_Begin', wb);
% setappdata(0, 'window_Dur', wd);
setappdata(0, 'm_h', matrix_h);

% Delete
function pushbutton5_Callback(hObject, eventdata, handles)
% Deleta 
temp_usvs = deleteUSV(getappdata(0, 'curated_usvs'), getappdata(0, 'k'));
itr = getappdata(0, 'k');
% Lan�a as vari�veis
setappdata(0, 'curated_usvs', temp_usvs);

% Mantem a janela
matrix_h = getappdata(0, 'm_h');
matrix_h(itr, :) = getPosition(getappdata(0, 'h'));
setappdata(0, 'm_h', matrix_h);

% Status
color = getappdata(0, 'c_h');
set(handles.text5, 'String', strcat('Status: Rejected'));
color(itr, :) = [1 0 0];
setappdata(0, 'c_h', color);


pushbutton2_Callback(hObject, eventdata, handles);
% Muda de USV
if (itr~=length(getappdata(0, 'idx_ini')))
    pushbutton1_Callback(hObject, eventdata, handles);
end

% Escolher o colormap
function popupmenu3_Callback(hObject, eventdata, handles)
contents = get(handles.popupmenu3,'String'); 
pop_choice = contents{get(handles.popupmenu3,'Value')};
switch pop_choice
    case 'parula'
        temp = parula;
    case 'hot'
        temp = hot;
    case 'gray'
        temp = flipud(gray);
    otherwise
        temp = copper;
end
setappdata(0, 'color_map', temp);
pushbutton2_Callback(hObject, eventdata, handles);

% Go to detection
function pushbutton12_Callback(hObject, eventdata, handles)
closereq;
setappdata(0, 'transition', 1);
Detection();

% Slider
function slider2_Callback(hObject, eventdata, handles)
new = int32(round(get(handles.slider2, 'Value')));
setappdata(0, 'k', new);
if (new==1)
    set(handles.pushbutton8,'Enable','off'); 
    set(handles.pushbutton1,'Enable','on'); 
elseif (new==length(getappdata(0, 'idx_ini')))
    set(handles.pushbutton8,'Enable','on'); 
    set(handles.pushbutton1,'Enable','off'); 
else
    set(handles.pushbutton8,'Enable','on'); 
    set(handles.pushbutton1,'Enable','on'); 
end
pushbutton2_Callback(hObject, eventdata, handles);

% New USV
function pushbutton15_Callback(hObject, eventdata, handles)
% Habilita ret�ngulo para USV extra
h_NEW=imrect(handles.axes3); %ret�ngulo novo
setColor(h_NEW, [0 0.6 0.8]);
setappdata(0, 'n_h', h_NEW);
setappdata(0, 'choice', 1);




function file_Callback(hObject, eventdata, handles)



% Save calls
function save_calls_Callback(hObject, eventdata, handles)
% Salva calls
save_calls(getappdata(0, 'n_cusv'), getappdata(0, 'n_wb'), getappdata(0, 'n_wd'),...
    getappdata(0, 'name'), getappdata(0, 'curated_usvs'), getappdata(0, 'window_Begin'),...
    getappdata(0, 'window_Dur'), getappdata(0, 'STEP'), getappdata(0, 'WIN'),...
    getappdata(0, 'params'), getappdata(0, 'thre'), getappdata(0, 'DUR'),...
    getappdata(0, 'ISI'), getappdata(0, 'INIF'), getappdata(0, 'FINF'),...
    getappdata(0, 'fs'), getappdata(0, 'audio_data'));



% Save session
function save_session_Callback(hObject, eventdata, handles)
% Salva sess�o
save_session(getappdata(0, 'name'), getappdata(0, 'k'), getappdata(0, 'curated_usvs'),...
    getappdata(0, 'window_Begin'), getappdata(0, 'window_Dur'), getappdata(0, 'n_cusv'),...
    getappdata(0, 'n_wb'), getappdata(0, 'n_wd'), getappdata(0, 'm_h'),...
    getappdata(0, 'c_h'), getappdata(0, 'n_k'),...
    getappdata(0, 'NEW_m_h'), getappdata(0, 'NEW_c_h'));

% Load session
function load_session_Callback(hObject, eventdata, handles)
[name, path] = uigetfile({'.mat'});
try
    file = fullfile(path, name);
    temp = load(file);
    setappdata(0, 'curated_usvs', temp.curated_usvs);
    setappdata(0, 'k', temp.k);
    setappdata(0, 'window_Begin', temp.window_Begin);
    setappdata(0, 'window_Dur', temp.window_Dur);
    setappdata(0, 'n_wb', temp.NEW_window_Begin);
    setappdata(0, 'n_wd', temp.NEW_window_Dur);
    setappdata(0, 'n_cusv', temp.NEW_curated_usvs);
    setappdata(0, 'n_k', temp.NEW_k);
    setappdata(0, 'm_h', temp.matrix_h);
    setappdata(0, 'c_h', temp.color_h);
    setappdata(0, 'NEW_m_h', temp.NEW_matrix_h);
    setappdata(0, 'NEW_c_h', temp.NEW_color_h);
    if(~isempty(temp.NEW_matrix_h)) set(handles.pushbutton17,'Enable','on'); end
    if(temp.k~=0) set(handles.pushbutton8,'Enable','on'); end
end
pushbutton2_Callback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)



% Curate new USVs
function pushbutton17_Callback(hObject, eventdata, handles)
closereq;
setappdata(0, 'transition3', 1);
Refine();


% Plot gui
function plot_Callback(hObject, eventdata, handles)
Plot();

% Analysis
function analysis_Callback(hObject, eventdata, handles)
Analysis();


function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
switch eventdata.Key
    case 'leftarrow' %previous
        pushbutton8_Callback(hObject, eventdata, handles)
    case 'rightarrow' %next
        pushbutton1_Callback(hObject, eventdata, handles);
    case 'r' %refresh
        pushbutton2_Callback(hObject, eventdata, handles)
    case 'x' %redefine
        pushbutton4_Callback(hObject, eventdata, handles)
    case 'z' %reject
        pushbutton5_Callback(hObject, eventdata, handles);
    case 'c' %confirm
        pushbutton3_Callback(hObject, eventdata, handles);
    case 'n' %new usv
        pushbutton15_Callback(hObject, eventdata, handles);
    end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
clear_variables();
delete(hObject);

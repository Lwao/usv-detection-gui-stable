function varargout = Audio_range(varargin)
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @Audio_range_OpeningFcn, ...
                       'gui_OutputFcn',  @Audio_range_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
end

function Audio_range_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.output = hObject;
    guidata(hObject, handles);

    audio_pathname = getappdata(0, 'audio_pathname');
    audio_name = getappdata(0, 'name');

    [audio_data,fs] = audioread([audio_pathname audio_name],'native');
    max_time = length(audio_data)/fs;
    
    clearvars audio_data fs audio_name audio_pathname
    
    if(isempty(getappdata(0,'time_range')))
        handles.initial_time = 0;
        handles.final_time = max_time;
    else
        temp = getappdata(0,'time_range');
        handles.initial_time = temp(1);
        handles.final_time = temp(2);        
        clearvars temp
    end
    
    set(handles.edit_initial_time, 'String', num2str(handles.initial_time));
    set(handles.edit_final_time, 'String', num2str(handles.final_time));
    set(handles.text_total_time, 'String', strcat('Total time: ', num2str(max_time), 's'));
    
    setappdata(0,'time_range',[handles.initial_time, handles.final_time])
    setappdata(0,'max_time', max_time);
    
    clearvars max_time
    guidata(hObject,handles)
end

function varargout = Audio_range_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    CREATE FCN    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function edit_initial_time_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

function edit_final_time_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%     CALLBACKS     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function edit_initial_time_Callback(hObject, eventdata, handles)
    input = str2double(get(handles.edit_initial_time, 'String'));
    if (isfinite(input)) 
        if(input>0 && input<handles.final_time && input<getappdata(0,'max_time'))
            handles.initial_time = input; 
        else
            warning('Invalid time range! Please change your parameters.');
        end
    else
        warning('Invalid time range! Please change your parameters.');
    end
    guidata(hObject,handles)
end

function edit_final_time_Callback(hObject, eventdata, handles)
    input = str2double(get(handles.edit_final_time, 'String'));
    if (isfinite(input)) 
        if(input>0 && input>handles.initial_time && input<getappdata(0,'max_time'))
            handles.final_time = input; 
        else
            warning('Invalid time range! Please change your parameters.');
        end
    else
        warning('Invalid time range! Please change your parameters.');
    end
%     if (isfinite(input)) handles.final_time = input; end
    guidata(hObject,handles)
end

function pushbutton_confirm_selection_Callback(hObject, eventdata, handles)
    setappdata(0,'time_range',[handles.initial_time, handles.final_time])
end

function varargout = Refine(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Refine_OpeningFcn, ...
                   'gui_OutputFcn',  @Refine_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end


function Refine_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

movegui(gcf,'center')

% Axes1
axes(handles.axes1)
intensity = 4e-8;
title(getappdata(0, 'name'))
hold on
box off
axis xy
ylabel('Frequency(kHz)')
caxis([0 intensity])
colormap(flipud(gray))
hold off
ylim([20 110])
% Axes2
axes(handles.axes2)
hold on
box off
axis xy
xlabel('Time (s)')
ylabel('Frequency(kHz)')
caxis([0 intensity])
colormap(flipud(gray))
hold off
ylim([20 110])

% USVs extras
n_cusv = getappdata(0, 'n_cusv');
n_wb = getappdata(0, 'n_wb');
n_wd = getappdata(0, 'n_wd');
n = length(n_wb);
%getappdata(0, 'n_k', []);

idxx = length(getappdata(0, 'n_wb'));
if (idxx==1)
    set(handles.pushbutton1,'Enable','off');
    set(handles.pushbutton3,'Enable','off');
end

% Inicializando slider
set(handles.slider1, 'Min', 1);
set(handles.slider1, 'Max', n);
set(handles.slider1, 'SliderStep', [1/n, 5/n]);
set(handles.slider1, 'Value', 1);

if(isempty(getappdata(0, 'NEW_m_h')))
% Inicializa as posi��es dos imrects
    matrix_h = zeros(n, 4);
    color_h = zeros(n, 3);
    color_h(1:end, 2) = 1; % todos cor verde claro (confirmado)
    setappdata(0, 'NEW_m_h', matrix_h);
    setappdata(0, 'NEW_c_h', color_h);
end

% Inicializa iterador
iterator = 1;
setappdata(0, 'i', iterator);
set(handles.pushbutton3,'Enable','off')

% Total de USVs
set(handles.text7, 'String', strcat('Total USVs: ', num2str(n)));


pushbutton2_Callback(hObject, eventdata, handles)
pushbutton2_Callback(hObject, eventdata, handles)

function varargout = Refine_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    CREATE FCN    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Spacer.
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Intensity
function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Color map
function popupmenu1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Slider
function slider1_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%     CALLBACKS     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Spacer
function edit1_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit1, 'String'));
if (isfinite(input)) 
    setappdata(0, 'spacer', input);
    pushbutton2_Callback(hObject, eventdata, handles);
end

% Intensity
function edit2_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit2, 'String'));
if (isfinite(input))    
    input = input*1e-8;
    setappdata(0, 'intensity', input);
    pushbutton2_Callback(hObject, eventdata, handles);
end

% Color map
function popupmenu1_Callback(hObject, eventdata, handles)
contents = get(handles.popupmenu1,'String'); 
pop_choice = contents{get(handles.popupmenu1,'Value')};
switch pop_choice
    case 'parula'
        temp = parula;
    case 'hot'
        temp = hot;
    case 'gray'
        temp = flipud(gray);
    otherwise
        temp = copper;
end
setappdata(0, 'color_map', temp);
pushbutton2_Callback(hObject, eventdata, handles);

% Next
function pushbutton1_Callback(hObject, eventdata, handles)
iterator = getappdata(0, 'i');
idx = length(getappdata(0, 'n_wb'));
% Desabilita o NEXT, caso esteja na �ltima USV
if (iterator < idx-1)
    iterator = iterator + 1;
else
    iterator = iterator + 1;
    set(handles.pushbutton1,'Enable','off');
end
% Habilita o PREVIOUS se estiver ap�s a primeira USV
if (iterator > 1)
    set(handles.pushbutton3,'Enable','on');
else
    set(handles.pushbutton3,'Enable','off');
end
setappdata(0, 'i', iterator);
set(handles.slider1, 'Value', iterator);
pushbutton2_Callback(hObject, eventdata, handles);


% Refresh
function pushbutton2_Callback(hObject, eventdata, handles)
% Pega as vari�veis necess�rias
spa = getappdata(0, 'spacer');
iny = getappdata(0, 'intensity');
S_t = getappdata(0, 'S');
tim = getappdata(0, 't');
fre = getappdata(0, 'f');
itr = getappdata(0, 'i');
col = getappdata(0, 'color_map');
ste = getappdata(0, 'STEP');
nam = getappdata(0, 'name');
h_t = getappdata(0, 'h');
h_m = getappdata(0, 'NEW_m_h');
color = getappdata(0, 'NEW_c_h');
NEW_cusv = getappdata(0, 'n_cusv');
NEW_window_Begin = getappdata(0, 'n_wb');
NEW_window_Dur = getappdata(0, 'n_wd');


% (col, h_m, name, color_select, k, spacer, intensity, S, t, f, fs, axes1, axes2)
% Inicia curagem
[mat_rect, rect, temp1, temp2, wb, wd] = NEWcurating(NEW_cusv, NEW_window_Begin, NEW_window_Dur, color, h_m, nam, col, itr, spa, iny, ste, S_t, tim, fre, handles.axes1, handles.axes2);
number = num2str(getappdata(0, 'i'), 5);
set(handles.text5, 'String', strcat('Putative USV no. ', number));

% Lan�a as vari�veis
setappdata(0, 'h', rect);
setappdata(0, 'NEW_m_h', mat_rect);
setappdata(0, 't1', temp1);
setappdata(0, 't2', temp2);
setappdata(0, 'n_wb', wb);
setappdata(0, 'n_wd', wd);

% Trata o status
curated = getappdata(0, 'n_cusv');
if ((curated(itr, 1)~=0) & (curated(itr, 2)==0)) %primeira diferente de 0 e segunda igual a 0
    set(handles.text6, 'String', strcat('Status: Rejected'));
elseif ((curated(itr, 1)~=0) & (curated(itr, 2)~=0)) %primeira e segunda coluna diferente de 0
    set(handles.text6, 'String', strcat('Status: Confirmed'));
end

% Previous
function pushbutton3_Callback(hObject, eventdata, handles)
iterator = getappdata(0, 'i');
idx = length(getappdata(0, 'n_wb'));
% Desabilita o PREVIOUS, caso esteja na primeira USV
if (iterator > 2)
    iterator = iterator - 1;
else
    iterator = iterator - 1;
    set(handles.pushbutton3,'Enable','off');
end
% Habilita o NEXT se estiver antes da �ltima USV
if (iterator < idx)
    set(handles.pushbutton1,'Enable','on')
else
    set(handles.pushbutton1,'Enable','off')
end
setappdata(0, 'i', iterator);
set(handles.slider1, 'Value', iterator);
pushbutton2_Callback(hObject, eventdata, handles);


% Confirm
function pushbutton4_Callback(hObject, eventdata, handles)
% Pega as vari�veis necess�rias
rect = getappdata(0, 'h');
usvs = getappdata(0, 'n_cusv');
itr = getappdata(0, 'i');
win_b = getappdata(0, 'n_wb');
win_d = getappdata(0, 'n_wd');
tempt = getappdata(0, 't');
tempt1 = getappdata(0, 't1');
tempt2 = getappdata(0, 't2');

% Confirma a sele��o
[curated_temp] = NEWconfirm(win_b, usvs, itr, rect);

% Lan�a vari�veis
setappdata(0, 'n_cusv', curated_temp);
% setappdata(0, 'n_wb', window_Begin);
% setappdata(0, 'n_wd', window_Dur);

% Status para nova confirma��o
color = getappdata(0, 'NEW_c_h');
color(itr, :) = [0 0.6 0];
setappdata(0, 'NEW_c_h', color);
set(handles.text6, 'String', strcat('Status: Confirmed'));

% Mantem a janela
matrix_h = getappdata(0, 'NEW_m_h');
matrix_h(itr, :) = getPosition(rect);
setappdata(0, 'NEW_m_h', matrix_h);

pushbutton2_Callback(hObject, eventdata, handles);

% Muda de USV
if (itr~=length(getappdata(0, 'n_wb')))
    pushbutton1_Callback(hObject, eventdata, handles);
end

% Redefine
function pushbutton5_Callback(hObject, eventdata, handles)
% Pega as vari�veis necess�rias
rect = getappdata(0, 'h');
usvs = getappdata(0, 'n_cusv');
itr = getappdata(0, 'i');
matrix_h = getappdata(0, 'NEW_m_h');
tempt = getappdata(0, 't');
tempt1 = getappdata(0, 't1');
tempt2 = getappdata(0, 't2');
win_begin = getappdata(0, 'n_wb');

% Redefine o ret�ngulo
[curated_temp, h_temp] = NEWredefine(win_begin, rect, usvs, itr, handles.axes1);
matrix_h(itr, :) = getPosition(h_temp);

% Lan�a vari�veis
setappdata(0, 'n_cusv', curated_temp);
setappdata(0, 'h', h_temp);
setappdata(0, 'NEW_m_h', matrix_h);

% Reject
function pushbutton6_Callback(hObject, eventdata, handles)
usvs = getappdata(0, 'n_cusv');
itr = getappdata(0, 'i');

% Deleta 
temp_usvs = deleteUSV(usvs, itr);

% Lan�a as vari�veis
setappdata(0, 'n_cusv', temp_usvs);

% Mantem a janela
matrix_h = getappdata(0, 'NEW_m_h');
matrix_h(itr, :) = getPosition(getappdata(0, 'h'));
setappdata(0, 'NEW_m_h', matrix_h);

% Status
color = getappdata(0, 'NEW_c_h');
set(handles.text6, 'String', strcat('Status: Rejected'));
color(itr, :) = [1 0 0];
setappdata(0, 'NEW_c_h', color);

pushbutton2_Callback(hObject, eventdata, handles);
% Muda de USV
if (itr~=length(getappdata(0, 'n_wb')))
    pushbutton1_Callback(hObject, eventdata, handles);
end


% Slider
function slider1_Callback(hObject, eventdata, handles)
new = int32(round(get(handles.slider1, 'Value')));
setappdata(0, 'i', new);
if (new==1)
    set(handles.pushbutton3,'Enable','off'); 
    set(handles.pushbutton1,'Enable','on'); 
elseif (new==length(getappdata(0, 'n_wb')))
    set(handles.pushbutton3,'Enable','on'); 
    set(handles.pushbutton1,'Enable','off'); 
else
    set(handles.pushbutton3,'Enable','on'); 
    set(handles.pushbutton1,'Enable','on'); 
end
pushbutton2_Callback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)


% Save session
function save_session_Callback(hObject, eventdata, handles)
% Pega as vari�veis necess�rias
win_b = getappdata(0, 'window_Begin');
win_d = getappdata(0, 'window_Dur');
usvs = getappdata(0, 'curated_usvs');
itr = getappdata(0, 'k');
nam = getappdata(0, 'name');
NEW_win_b = getappdata(0, 'n_wb');
NEW_win_d = getappdata(0, 'n_wd');
NEW_usvs = getappdata(0, 'n_cusv');
matrix_h = getappdata(0, 'm_h');
color_h = getappdata(0, 'c_h');
NEW_k = getappdata(0, 'n_k');
NEW_matrix_h = getappdata(0, 'NEW_m_h');
NEW_color_h = getappdata(0, 'NEW_c_h');


% Salva sess�o
save_session(nam, itr, usvs, win_b, win_d, NEW_usvs, NEW_win_b, NEW_win_d, matrix_h, color_h, NEW_k, NEW_matrix_h, NEW_color_h);


% Save calls
function save_calls_Callback(hObject, eventdata, handles)
% Pega as vari�veis necess�rias
win_b = getappdata(0, 'window_Begin');
win_d = getappdata(0, 'window_Dur');
usvs = getappdata(0, 'curated_usvs');
nam = getappdata(0, 'name');
NEW_usvs = getappdata(0, 'n_cusv');
NEW_wb = getappdata(0, 'n_wb');
NEW_wd = getappdata(0, 'n_wd');

t_1 = getappdata(0, 'STEP');
t_2 = getappdata(0, 'WIN');
t_3 = getappdata(0, 'params');
t_4 = getappdata(0, 'thre');
t_5 = getappdata(0, 'DUR');
t_6 = getappdata(0, 'ISI');
t_7 = getappdata(0, 'INIF');
t_8 = getappdata(0, 'FINF');
t_9 = getappdata(0, 'fs');
t_10 = getappdata(0, 'audio_data');


% Salva calls
save_calls(NEW_usvs, NEW_wb, NEW_wd, nam, usvs, win_b, win_d, t_1, t_2, t_3, t_4, t_5, t_6, t_7, t_8, t_9, t_10);


% Main curating
function pushbutton10_Callback(hObject, eventdata, handles)
closereq;
Curating();


% Plot gui
function plot_Callback(hObject, eventdata, handles)
Plot();


% Analysis
function analysis_Callback(hObject, eventdata, handles)
Analysis()

% -------------------------------------
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
switch eventdata.Key
    case 'leftarrow' %previous
        pushbutton3_Callback(hObject, eventdata, handles)
    case 'rightarrow' %next
        pushbutton1_Callback(hObject, eventdata, handles);
    case 'r' %refresh
        pushbutton2_Callback(hObject, eventdata, handles)
    case 'x' %redefine
        pushbutton5_Callback(hObject, eventdata, handles)
    case 'z' %reject
        pushbutton6_Callback(hObject, eventdata, handles);
    case 'c' %confirm
        pushbutton4_Callback(hObject, eventdata, handles);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
clear_variables();
delete(hObject);

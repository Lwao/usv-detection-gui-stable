function varargout = Frequency_range(varargin)
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @Frequency_range_OpeningFcn, ...
                       'gui_OutputFcn',  @Frequency_range_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
end

function Frequency_range_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.output = hObject;
    guidata(hObject, handles);
    
    if(isempty(getappdata(0,'fs')))
        audio_pathname = getappdata(0, 'audio_pathname');
        audio_name = getappdata(0, 'name');

        [~,fs] = audioread([audio_pathname audio_name],'native');
        max_freq = fs/2/1000; % half sampling frequency [kHz]

        clearvars audio_data audio_name audio_pathname
    else
        max_freq = getappdata(0,'fs')/2/1000;
    end
    
    if(isempty(getappdata(0,'freq_range')))
        handles.initial_freq = 0;
        handles.final_freq = max_freq;
    else
        temp = getappdata(0,'freq_range');
        handles.initial_freq = temp(1);
        handles.final_freq = temp(2);        
        clearvars temp
    end
    
    set(handles.edit_initial_freq, 'String', num2str(handles.initial_freq));
    set(handles.edit_final_freq, 'String', num2str(handles.final_freq));
    set(handles.text_max_freq, 'String', strcat('Maximum frequency: ', num2str(max_freq), 'kHz'));
    
    setappdata(0,'freq_range',[handles.initial_freq, handles.final_freq])
    setappdata(0,'max_freq', max_freq);
    
    clearvars max_freq
    guidata(hObject,handles)
end

function varargout = Frequency_range_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    CREATE FCN    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function edit_initial_freq_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

function edit_final_freq_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%     CALLBACKS     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function edit_initial_freq_Callback(hObject, eventdata, handles)
    input = str2double(get(handles.edit_initial_freq, 'String'));
    if (isfinite(input)) 
        if(input>=0 && input<handles.final_freq && input<getappdata(0,'max_freq'))
            handles.initial_freq = input; 
        else
            warning('Invalid frequency range! Please change your parameters.');
        end
    else
        warning('Invalid frequency range! Please change your parameters.');
    end
    guidata(hObject,handles)
end

function edit_final_freq_Callback(hObject, eventdata, handles)
    input = str2double(get(handles.edit_final_freq, 'String'));
    if (isfinite(input)) 
        if(input>=0 && input>handles.initial_freq && input<=getappdata(0,'max_freq'))
            handles.final_freq = input; 
        else
            warning('Invalid frequency range! Please change your parameters.');
        end
    else
        warning('Invalid frequency range! Please change your parameters.');
    end
%     if (isfinite(input)) handles.final_freq = input; end
    guidata(hObject,handles)
end

function pushbutton_confirm_selection_Callback(hObject, eventdata, handles)
    setappdata(0,'freq_range',[handles.initial_freq, handles.final_freq])
end

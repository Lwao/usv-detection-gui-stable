function varargout = Detection(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Detection_OpeningFcn, ...
                   'gui_OutputFcn',  @Detection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function Detection_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

movegui(gcf,'center')

set(handles.pushbutton2,'Enable','off')
set(handles.pushbutton5,'Enable','off')
set(handles.pushbutton6,'Enable','off')
set(handles.pushbutton_confirmf,'Enable','off')
set(handles.pushbutton_confirmt,'Enable','off')

%Axes2
setappdata(0, 'cm_detection', flipud(gray));
setappdata(0, 'cm_intensity', 4e-8);
axes(handles.axes2)
axis xy; 
xlabel('Time (s)')
ylabel('Frequency (kHz)')
caxis([0 getappdata(0, 'cm_intensity')])
colormap(getappdata(0, 'cm_detection'))
box off
if(getappdata(0, 'transition')==0)
    setappdata(0, 'entropy_select', 0)
end

if (getappdata(0, 'transition')==1)
    set(handles.pushbutton2,'Enable','on');
    set(handles.pushbutton5,'Enable','on');
    set(handles.pushbutton6,'Enable','on');
    set(handles.pushbutton_confirmf,'Enable','on')
    set(handles.pushbutton_confirmt,'Enable','on')
    axes(handles.axes2)
    imagesc(getappdata(0, 't'),getappdata(0, 'f')/1000,getappdata(0, 'S')');
    axis xy; 
    xlabel('Time (s)')
    ylabel('Frequency (kHz)')
    caxis([0 getappdata(0, 'cm_intensity')])
    colormap(getappdata(0, 'cm_detection'))
    box off
    
    set(handles.popupmenu1, 'String', getappdata(0, 'audio_fname'));
    set(handles.edit1, 'String', 1e3*getappdata(0, 'WIN'));
    set(handles.edit2, 'String', 1e3*getappdata(0, 'STEP'));
    set(handles.edit5, 'String', getappdata(0, 'INIF'));
    set(handles.edit6, 'String', getappdata(0, 'FINF'));
    set(handles.edit7, 'String', getappdata(0, 'THRET'));
    set(handles.edit12, 'String', getappdata(0, 'THREF'));
    set(handles.edit8, 'String', getappdata(0, 'ISI'));
    set(handles.edit9, 'String', getappdata(0, 'DUR'));
    set(handles.edit13, 'String', 1e8*getappdata(0, 'cm_intensity'));

    if(getappdata(0, 'entropy_select')==0) %Wiener
        set(handles.uibuttongroup1,'SelectedObject',handles.radiobutton1)
    end
    if(getappdata(0, 'entropy_select')==1)%shannon
        set(handles.uibuttongroup1,'SelectedObject',handles.radiobutton1)
        set(handles.uibuttongroup1,'SelectedObject',handles.radiobutton2)
    end
    

    handles = initialize_TF_range(getappdata(0,'name'),handles);
    guidata(hObject,handles)

    
    setappdata(0, 'transition', 0);
end


function varargout = Detection_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%    CREATE FCN    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Audio selection pop-up
function popupmenu1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Win
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Step
function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Initial Frequency
function edit5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Final Frequency
function edit6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Time Threshold
function edit7_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Frequency Threshold
function edit12_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Min. interval to accept distinct vocalizations (ms)
function edit8_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Min. duration to be considered USV (ms)
function edit9_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Spacer
function edit10_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Intensity
function edit11_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Intensity color map
function edit13_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Color map
function popupmenu4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_tstart_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_tend_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_fstart_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_fend_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%     CALLBACKS     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Spectogram
function pushbutton2_Callback(hObject, eventdata, handles)
% Calculate spectrogram with 'spectogram' function
[elap, spec, time, freq, par, fs_temp, adata, audiodouble] = spectogram(...
    getappdata(0, 'audio_pathname'), getappdata(0, 'name'), getappdata(0, 'WIN'),...
    getappdata(0, 'STEP'), getappdata(0,'time_range'), getappdata(0,'freq_range'));


% Lan�a as vari�veis
elap = num2str(round(elap,2), 5);
setappdata(0, 'S', spec);
setappdata(0, 't', time);
setappdata(0, 'f', freq);
setappdata(0, 'params', par);
setappdata(0, 'fs', fs_temp);
setappdata(0, 'audio_data', adata);
setappdata(0, 'audio_double', audiodouble);

set_spec(getappdata(0, 'S'), getappdata(0, 'f'), getappdata(0, 't'),...
    getappdata(0, 'cm_intensity'), getappdata(0, 'cm_detection'),...
    getappdata(0, 'freq_range'), handles.axes2);

set(handles.text4, 'String', strcat('Elapsed time: ', elap, 's'));
set(handles.text16, 'String', strcat('Audio file sampling rate: ', num2str(fs_temp/1000, 6), 'kHz'));
set(handles.pushbutton5,'Enable','on')

% Audio selection pop-up
function popupmenu1_Callback(hObject, eventdata, handles)
% Pega os nomes dos arquivos para mostrar
fname = getappdata(0, 'audio_fname');
set(hObject, 'String', fname);

% Seleciona o nome do arquivo
contents = get(handles.popupmenu1,'String');
pop_choice = contents{get(handles.popupmenu1,'Value')};
setappdata(0, 'name', pop_choice);
set(handles.pushbutton2,'Enable','on')
setappdata(0,'time_range', [])
setappdata(0,'frequency_range', [])
handles = initialize_TF_range(pop_choice,handles);
guidata(hObject,handles)




% Win
function edit1_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit1, 'String'));
if (isfinite(input)) setappdata(0, 'WIN', 1e-3*input), end

% Step
function edit2_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit2, 'String'));
if (isfinite(input)) setappdata(0, 'STEP', 1e-3*input), end

% Initial Frequency
function edit5_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit5, 'String'));
if (isfinite(input)) setappdata(0, 'INIF', input), end

% Final Frequency
function edit6_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit6, 'String'));
if (isfinite(input)) setappdata(0, 'FINF', input), end

% Time Threshold
function edit7_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit7, 'String'));
if (isfinite(input)) setappdata(0, 'THRET', input), end

% Frequency Threshold
function edit12_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit12, 'String'));
if (isfinite(input)) setappdata(0, 'THREF', input), end

% Min. interval to accept distinct vocalizations (ms)
function edit8_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit8, 'String'));
if (isfinite(input)) setappdata(0, 'ISI', input), end
    
% Min. duration to be considered USV (ms)
function edit9_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit9, 'String'));
if (isfinite(input)) setappdata(0, 'DUR', input), end

% Intensity color map
function edit13_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit13, 'String'));
if (isfinite(input))    
    input = input*1e-8;
    setappdata(0, 'cm_intensity', input);
end
set_spec(getappdata(0, 'S'), getappdata(0, 'f'), getappdata(0, 't'),...
    getappdata(0, 'cm_intensity'), getappdata(0, 'cm_detection'),...
    getappdata(0, 'freq_range'), handles.axes2);

% Detect
function pushbutton5_Callback(hObject, eventdata, handles)
% Detecta as USVs
[elap, idx_ini_t, idx_end_t, thre_t, Entropy_t, rms_entropy_t, USV_data_t] = detection(...
    getappdata(0, 'INIF'), getappdata(0, 'FINF'), getappdata(0, 'THRET'), getappdata(0, 'THREF'),...
    getappdata(0, 'ISI'), getappdata(0, 'DUR'), getappdata(0, 'S'), getappdata(0, 'STEP'),...
    getappdata(0, 'f'), getappdata(0, 'entropy_select'));
elap = num2str(round(elap,2), 5);
set(handles.text13, 'String', strcat('Elapsed time: ', elap, 's'));

% Lan�ando as vari�veis
setappdata(0, 'idx_ini', idx_ini_t);
setappdata(0, 'idx_end', idx_end_t);
setappdata(0, 'thre', thre_t);
setappdata(0, 'Entropy', Entropy_t);
setappdata(0, 'rms_entropy', rms_entropy_t);
setappdata(0, 'USV_data', USV_data_t);

set(handles.pushbutton6,'Enable','on')

% Go to curating
function pushbutton6_Callback(hObject, eventdata, handles)
closereq;
setappdata(0, 'transition3', 0);
Curating();



function file_Callback(hObject, eventdata, handles)



% New project
function new_Callback(hObject, eventdata, handles)
setappdata(0, 'transition', 0);
closereq;
Detection();


% Load audio
function load_audio_Callback(hObject, eventdata, handles)
% Carrega os arquivos de �udio
[audio_fname_temp, audio_pathname_temp] = load_sound_file();

% Lan�a as vari�veis
setappdata(0, 'audio_fname', audio_fname_temp);
setappdata(0, 'audio_pathname', audio_pathname_temp);

% Valores padr�o
setappdata(0, 'WIN', 0.01);
setappdata(0, 'STEP', 0.001);
setappdata(0, 'INIF', 18000);
setappdata(0, 'FINF', 125000);
setappdata(0, 'THRET', 0.5);
setappdata(0, 'THREF', 0.25);
setappdata(0, 'ISI', 20);
setappdata(0, 'DUR', 3);
setappdata(0, 'spacer', 0.1);
setappdata(0, 'intensity', 0.1e-8);
setappdata(0, 'color_map', (flipud(gray)))
try
    setappdata(0, 'name', audio_fname_temp(1, 1));
end

% Define os valores do pop-up
set(handles.popupmenu1, 'String', audio_fname_temp);
fname = getappdata(0, 'audio_fname'); 
set(handles.popupmenu1, 'String', fname);
set(handles.pushbutton2,'Enable','on')
contents = get(handles.popupmenu1,'String'); 
pop_choice = contents{get(handles.popupmenu1,'Value')};
setappdata(0, 'name', pop_choice);
setappdata(0,'time_range', [])
setappdata(0,'frequency_range', [])
handles = initialize_TF_range(pop_choice,handles);
guidata(hObject,handles)



% Load parameters
function load_par_Callback(hObject, eventdata, handles)
[name, path] = uigetfile({'.mat'});
try
    file = fullfile(path, name);
    temp = load(file);
    setappdata(0, 'audio_pathname', temp.audio_pathname);
    setappdata(0, 'audio_fname', temp.audio_fname);
    setappdata(0, 'name', temp.name);
    setappdata(0, 'WIN', temp.win);
    setappdata(0, 'STEP', temp.step);
    setappdata(0, 'INIF', temp.f_ini);
    setappdata(0, 'FINF', temp.f_end);
    setappdata(0, 'THRET', temp.thre_const);
    setappdata(0, 'THREF', temp.thre_const_freq)
    setappdata(0, 'ISI', temp.isi);
    setappdata(0, 'DUR', temp.dur_min);
    
    % Define os valores do pop-up
    set(handles.popupmenu1, 'String', temp.audio_fname);
    
    set(handles.edit1, 'String', 1e3*temp.win);
    set(handles.edit2, 'String', 1e3*temp.step);
    set(handles.edit5, 'String', temp.f_ini);
    set(handles.edit6, 'String', temp.f_end);
    set(handles.edit7, 'String', temp.thre_const);
    set(handles.edit8, 'String', temp.isi);
    set(handles.edit9, 'String', temp.dur_min);
    set(handles.edit12, 'String', temp.thre_const_freq);
end


% Save parameters
function save_par_Callback(hObject, eventdata, handles)
name = getappdata(0, 'name');
[file,path,indx] = uiputfile(['Parameters_' '.mat']);
audio_pathname = getappdata(0, 'audio_pathname');
audio_fname = getappdata(0, 'audio_fname');
win = getappdata(0, 'WIN');
step = getappdata(0, 'STEP');
f_ini =getappdata(0, 'INIF');
f_end = getappdata(0, 'FINF');
thre_const = getappdata(0, 'THRET');
isi = getappdata(0, 'ISI');
dur_min = getappdata(0, 'DUR');
thre_const_freq = getappdata(0, 'THREF');

save([path file],'audio_pathname','audio_fname','name','win','step','f_ini','f_end', 'thre_const', 'thre_const_freq', 'isi', 'dur_min');


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function figure1_KeyPressFcn(hObject, eventdata, handles)



% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
    switch eventdata.Key
      case 'o'
          load_audio_Callback(hObject, eventdata, handles);
      case 'g'
          pushbutton2_Callback(hObject, eventdata, handles);
      case 'd'
          pushbutton5_Callback(hObject, eventdata, handles);  
      case 'c'
          pushbutton6_Callback(hObject, eventdata, handles);
    end







% Color map
function popupmenu4_Callback(hObject, eventdata, handles)
contents = get(handles.popupmenu4,'String'); 
pop_choice = contents{get(handles.popupmenu4,'Value')};
switch pop_choice
    case 'parula'
        temp = parula;
    case 'hot'
        temp = hot;
    case 'gray'
        temp = flipud(gray);
    otherwise
        temp = copper;
end
setappdata(0, 'cm_detection', temp);
set_spec(getappdata(0, 'S'), getappdata(0, 'f'), getappdata(0, 't'),...
    getappdata(0, 'cm_intensity'), getappdata(0, 'cm_detection'),...
    getappdata(0, 'freq_range'), handles.axes2);

% Wiener
function radiobutton1_Callback(hObject, eventdata, handles)
setappdata(0, 'entropy_select', 0)



% Shannon
function radiobutton2_Callback(hObject, eventdata, handles)
setappdata(0, 'entropy_select', 1)


% --------------------------------------------------------------------
function edit_Callback(hObject, eventdata, handles)
% hObject    handle to edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function audio_range_Callback(hObject, eventdata, handles)
Audio_range();


% --------------------------------------------------------------------
function frequency_range_Callback(hObject, eventdata, handles)
Frequency_range();


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
clear_variables();
delete(hObject);







function edit_tstart_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit_tstart, 'String'));
if (isfinite(input)) 
    if(input>0 && input<handles.final_time && input<getappdata(0,'max_time'))
        handles.initial_time = input; 
    else
        warning('Invalid time range! Please change your parameters.');
    end
else
    warning('Invalid time range! Please change your parameters.');
end
guidata(hObject,handles)

function edit_tend_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit_tend, 'String'));
if (isfinite(input)) 
    if(input>0 && input>handles.initial_time && input<getappdata(0,'max_time'))
        handles.final_time = input; 
    else
        warning('Invalid time range! Please change your parameters.');
    end
else
    warning('Invalid time range! Please change your parameters.');
end
%     if (isfinite(input)) handles.final_time = input; end
guidata(hObject,handles)


function pushbutton_confirmt_Callback(hObject, eventdata, handles)
setappdata(0,'time_range',[handles.initial_time, handles.final_time])

function edit_fstart_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit_fstart, 'String'));
if (isfinite(input)) 
    if(input>=0 && input<handles.final_freq && input<getappdata(0,'max_freq'))
        handles.initial_freq = input; 
    else
        warning('Invalid frequency range! Please change your parameters.');
    end
else
    warning('Invalid frequency range! Please change your parameters.');
end
guidata(hObject,handles)

function edit_fend_Callback(hObject, eventdata, handles)
input = str2double(get(handles.edit_fend, 'String'));
if (isfinite(input)) 
    if(input>=0 && input>handles.initial_freq && input<=getappdata(0,'max_freq'))
        handles.final_freq = input; 
    else
        warning('Invalid frequency range! Please change your parameters.');
    end
else
    warning('Invalid frequency range! Please change your parameters.');
end
%     if (isfinite(input)) handles.final_freq = input; end
guidata(hObject,handles)

function pushbutton_confirmf_Callback(hObject, eventdata, handles)
setappdata(0,'freq_range',[handles.initial_freq, handles.final_freq])

